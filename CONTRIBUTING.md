# Contributing

To contribute to this repository, send a mail at hermansendev@gmail.com and ask 
for developer access. Include gitlab comicId, otherwise your request won't be
considered.

## Feature

For a requested feature, open a feature branch (feature/<requested-feature>), 
that matches that feature, open a Merge Request, when you're ready for a review. 
For a Merge Request to be considered, it has to pass an internal review, and 
pass our pipeline, tests & deployment.

## Bugfix

Open a bugfix branch (bug/<bug-identifier>) then create a Merge Request, when
done. Suitable evidence has to be documented for the Merge request to be added.

## Suggesting features

Open an issue with the request feature, be sure to include the tag feature or 
bug as is.

## Alternatives

Fork the project, and open a remote Merge Request, same rules apply as above.