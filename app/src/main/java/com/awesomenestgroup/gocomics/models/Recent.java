package com.awesomenestgroup.gocomics.models;

public class Recent {
    private String title;
    private String id;
    private String lastChapterRead;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLastChapterRead() {
        return lastChapterRead;
    }

    public void setLastChapterRead(String lastChapterRead) {
        this.lastChapterRead = lastChapterRead;
    }
}
