package com.awesomenestgroup.gocomics.models;

import java.util.ArrayList;
import java.util.List;

public class Comic {
    private String title;
    private String comicId;
    private List<ChapterKeyPair> chapters = new ArrayList<>();
    public Comic(String title, String comicId) {
        this.title = title;
        this.comicId = comicId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getComicId() {
        return comicId;
    }

    public void setComicId(String comicId) {
        this.comicId = comicId;
    }

    public List<ChapterKeyPair> getChapters() {
        return chapters;
    }

    public void setChapters(List<ChapterKeyPair> chapters) {
        this.chapters = chapters;
    }

    public static class ChapterKeyPair {
        private String title;
        private String id;

        public ChapterKeyPair(String title, String id) {
            this.setTitle(title);
            this.setId(id);
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}

