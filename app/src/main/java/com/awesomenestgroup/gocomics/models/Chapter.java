package com.awesomenestgroup.gocomics.models;

public class Chapter {
    private final String title;

    public Chapter(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}

