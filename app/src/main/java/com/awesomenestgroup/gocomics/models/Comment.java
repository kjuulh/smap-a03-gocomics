package com.awesomenestgroup.gocomics.models;

public class Comment {
    private final String username;
    private final String commentText;

    public Comment(String username, String commentText) {
        this.username = username;
        this.commentText = commentText;
    }

    public String getUsername() {
        return username;
    }

    public String getCommentText() {
        return commentText;
    }
}
