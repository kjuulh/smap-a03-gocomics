package com.awesomenestgroup.gocomics.models;

public class Bookmark {
    private String comicTitle;
    private String comicId;
    private String mostCurrentChapterTitle;
    private String mostCurrentChapterId;
    private String latestReadTitle;
    private String latestReadId;
    private String firstTitle;
    private String firstId;

    public String getComicTitle() {
        return comicTitle;
    }

    public void setComicTitle(String comicTitle) {
        this.comicTitle = comicTitle;
    }

    public String getComicId() {
        return comicId;
    }

    public void setComicId(String comicId) {
        this.comicId = comicId;
    }

    public String getMostCurrentChapterTitle() {
        return mostCurrentChapterTitle;
    }

    public void setMostCurrentChapterTitle(String mostCurrentChapterTitle) {
        this.mostCurrentChapterTitle = mostCurrentChapterTitle;
    }

    public String getMostCurrentChapterId() {
        return mostCurrentChapterId;
    }

    public void setMostCurrentChapterId(String mostCurrentChapterId) {
        this.mostCurrentChapterId = mostCurrentChapterId;
    }

    public String getLatestReadTitle() {
        return latestReadTitle;
    }

    public void setLatestReadTitle(String latestReadTitle) {
        this.latestReadTitle = latestReadTitle;
    }

    public String getLatestReadId() {
        return latestReadId;
    }

    public void setLatestReadId(String latestReadId) {
        this.latestReadId = latestReadId;
    }

    public String getFirstTitle() {
        return firstTitle;
    }

    public void setFirstTitle(String firstTitle) {
        this.firstTitle = firstTitle;
    }

    public String getFirstId() {
        return firstId;
    }

    public void setFirstId(String firstId) {
        this.firstId = firstId;
    }
}

