package com.awesomenestgroup.gocomics.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MangaDetailsModel {

    @SerializedName("artist")
    private String artist;
    @SerializedName("artist_kw")
    private ArrayList<String> artist_kw;
    @SerializedName("author")
    private String author;
    @SerializedName("author_kw")
    private ArrayList<String> alias;
    @SerializedName("categories")
    private List<String> categories;
    @SerializedName("chapters")
    private ArrayList<ChapterModel> chapters;
    @SerializedName("chapters_len")
    private String chaptersLen;
    @SerializedName("created")
    private String created;
    @SerializedName("description")
    private String description;
    @SerializedName("image")
    private String image;
    @SerializedName("last_chapter_date")
    private String last_chapter_date;
    @SerializedName("title")
    private String title;
    @SerializedName("title_kw")
    private ArrayList<String> title_kw;


    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public ArrayList<String> getArtist_kw() {
        return artist_kw;
    }

    public void setArtist_kw(ArrayList<String> artist_kw) {
        this.artist_kw = artist_kw;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public ArrayList<String> getAlias() {
        return alias;
    }

    public void setAlias(ArrayList<String> alias) {
        this.alias = alias;
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public ArrayList<ChapterModel> getChapters() {
        return chapters;
    }

    public void setChapters(ArrayList<ChapterModel> chapters) {
        this.chapters = chapters;
    }

    public String getChaptersLen() {
        return chaptersLen;
    }

    public void setChaptersLen(String chaptersLen) {
        this.chaptersLen = chaptersLen;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLast_chapter_date() {
        return last_chapter_date;
    }

    public void setLast_chapter_date(String last_chapter_date) {
        this.last_chapter_date = last_chapter_date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<String> getTitle_kw() {
        return title_kw;
    }

    public void setTitle_kw(ArrayList<String> title_kw) {
        this.title_kw = title_kw;
    }
}
