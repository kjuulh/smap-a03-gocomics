package com.awesomenestgroup.gocomics.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MangaInfoModel {

    @SerializedName("t")
    private String title;
    @SerializedName("im")
    private String imageURI;
    @SerializedName("i")
    private String mangaID;
    @SerializedName("a")
    private String alias;
    @SerializedName("s")
    private String status;
    @SerializedName("c")
    private ArrayList<String> category;
    @SerializedName("ld")
    private String last_chapter_date;
    @SerializedName("h")
    private String hits;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageURI() {
        return imageURI;
    }

    public void setImageURI(String imageURI) {
        this.imageURI = imageURI;
    }

    public String getMangaID() {
        return mangaID;
    }

    public void setMangaID(String mangaID) {
        this.mangaID = mangaID;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<String> getCategory() {
        return category;
    }

    public void setCategory(ArrayList<String> category) {
        this.category = category;
    }

    public String getLast_chapter_date() {
        return last_chapter_date;
    }

    public void setLast_chapter_date(String last_chapter_date) {
        this.last_chapter_date = last_chapter_date;
    }
}
