package com.awesomenestgroup.gocomics.models;

import android.graphics.Bitmap;

public class Page {
    private int page_number;
    private String page_image_id;
    private int image_width;
    private int image_height;
    private Bitmap page;

    public int getPage_number() {
        return page_number;
    }

    public void setPage_number(int page_number) {
        this.page_number = page_number;
    }

    public String getPage_image_id() {
        return page_image_id;
    }

    public void setPage_image_id(String page_image_id) {
        this.page_image_id = page_image_id;
    }

    public int getImage_width() {
        return image_width;
    }

    public void setImage_width(int image_width) {
        this.image_width = image_width;
    }

    public int getImage_height() {
        return image_height;
    }

    public void setImage_height(int image_height) {
        this.image_height = image_height;
    }

    public Bitmap getBitmap() {
        return page;
    }

    public void setPage(Bitmap page) {
        this.page = page;
    }
}
