package com.awesomenestgroup.gocomics.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ChapterDetailsModel {
    @SerializedName("images")
    private ArrayList<Page> imagesModels;

    public ArrayList<Page> getImagesModels() {
        return imagesModels;
    }

    public void setImagesModels(ArrayList<Page> imagesModels) {
        this.imagesModels = imagesModels;
    }
}
