package com.awesomenestgroup.gocomics.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MangaListModel {
    @SerializedName("manga")
    private List<MangaInfoModel> mangas;

    public List<MangaInfoModel> getMangas() {
        return mangas;
    }

    public void setMangas(List<MangaInfoModel> mangas) {
        this.mangas = mangas;
    }
}
