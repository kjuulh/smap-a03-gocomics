package com.awesomenestgroup.gocomics.validator;

import android.os.AsyncTask;
import android.util.Log;

import com.awesomenestgroup.gocomics.listeners.OnCollectionFetchedListener;
import com.awesomenestgroup.gocomics.providers.DatabaseProvider;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

public class MangaValidator {

    private static MangaValidator instance;
    private final String BLACKLIST_COLLECTION = "blacklist";
    private String TAG = "MangaValidator";
    private DatabaseProvider mDatabaseProvider;
    private List<String> mBlacklistedMangas;

    private MangaValidator() {
        mDatabaseProvider = DatabaseProvider.getInstance();

        fillBlacklist(new OnCollectionFetchedListener() {
            @Override
            public void getResults(Object object) {
                mBlacklistedMangas = (List<String>) object;
            }
        });
    }

    public static MangaValidator getInstance() {
        if (instance == null) {
            instance = new MangaValidator();
        }
        return instance;
    }

    private void fillBlacklist(final OnCollectionFetchedListener listener) {
        class GetAllCommentsTask extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                // Create reference to Firestore document

                mDatabaseProvider.getDb()
                        .collection(BLACKLIST_COLLECTION)
                        .addSnapshotListener(new EventListener<QuerySnapshot>() {
                            @Override
                            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                                if (queryDocumentSnapshots != null) {
                                    Log.d(TAG, "observeBlacklist: success");
                                    List<DocumentSnapshot> snapshots = queryDocumentSnapshots.getDocuments();

                                    List<String> blacklistedMangas = new ArrayList<>();

                                    for (DocumentSnapshot snapshot :
                                            snapshots) {
                                        String blacklistedManga = (String) snapshot.get("mangaId");
                                        if (blacklistedManga != null) {
                                            blacklistedMangas.add(blacklistedManga);
                                        }
                                    }

                                    listener.getResults(blacklistedMangas);
                                }
                            }
                        });
                return null;
            }
        }

        new GetAllCommentsTask().execute();
    }

    public boolean IsInBlacklist(String mangaId) {
        if (mBlacklistedMangas != null) {

            if (mBlacklistedMangas.contains(mangaId)) {
                Log.d(TAG, "IsInBlacklist: blacklisted " + mangaId);
                return true;
            } else {
                return false;
            }
        }
        Log.d(TAG, "IsInBlacklist: mBlacklistedMangas null");
        return false;
    }


}
