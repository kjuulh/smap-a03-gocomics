package com.awesomenestgroup.gocomics.providers;

import android.util.Log;

import com.google.firebase.firestore.FirebaseFirestore;

/**
 * Helper repository to manage data from Google Firestore belonging to GoComics
 */
public class DatabaseProvider {

    private static DatabaseProvider instance;
    private final String TAG = "DatabaseProvider";
    private FirebaseFirestore db;

    private DatabaseProvider() {
        db = FirebaseFirestore.getInstance();
    }

    public static DatabaseProvider getInstance() {
        if (instance == null) {
            instance = new DatabaseProvider();
        }
        return instance;
    }

    public FirebaseFirestore getDb() {
        if (db == null) {
            Log.d(TAG, "getDb: Database was null");
            return null;
        }
        return db;
    }
}
