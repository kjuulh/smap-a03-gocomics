package com.awesomenestgroup.gocomics.providers;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class UserProvider {

    private static UserProvider instance;
    private final FirebaseAuth mAuth;

    protected UserProvider() {
        mAuth = FirebaseAuth.getInstance();
    }

    public static UserProvider getInstance() {
        if (instance == null) {
            instance = new UserProvider();
        }
        return instance;
    }

    public boolean isLoggedIn() {
        FirebaseUser user = mAuth.getCurrentUser();
        return user != null;
    }

    public String getUserId() {
        if (isLoggedIn()) {
            FirebaseUser user = mAuth.getCurrentUser();
            return user.getUid();
        }
        return null;
    }

    public FirebaseUser getUser() {
        if (isLoggedIn()) {
            return mAuth.getCurrentUser();
        }
        return null;
    }
}
