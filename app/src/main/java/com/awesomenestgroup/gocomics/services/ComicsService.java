package com.awesomenestgroup.gocomics.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.lifecycle.MutableLiveData;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.awesomenestgroup.gocomics.MainActivity;
import com.awesomenestgroup.gocomics.R;
import com.awesomenestgroup.gocomics.models.ChapterDetailsModel;
import com.awesomenestgroup.gocomics.models.ChapterModel;
import com.awesomenestgroup.gocomics.models.MangaDetailsModel;
import com.awesomenestgroup.gocomics.models.MangaInfoModel;
import com.awesomenestgroup.gocomics.models.MangaListModel;
import com.awesomenestgroup.gocomics.models.Page;
import com.awesomenestgroup.gocomics.utility.ApiSingleton;
import com.awesomenestgroup.gocomics.utility.ChapterDeserializer;
import com.awesomenestgroup.gocomics.utility.PageDeserializer;
import com.awesomenestgroup.gocomics.utility.ViewModelBuilder;
import com.awesomenestgroup.gocomics.validator.MangaValidator;
import com.awesomenestgroup.gocomics.viewModels.models.ChapterViewModel;
import com.awesomenestgroup.gocomics.viewModels.models.DetailViewModel;
import com.awesomenestgroup.gocomics.viewModels.models.GalleryViewModel;
import com.awesomenestgroup.gocomics.viewModels.models.PageViewModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

// Uses the mangaeden public api
// Link: https://www.mangaeden.com/api/

public class ComicsService extends Service {

    private final IBinder binder = new ComicsServiceBinder();
    private final String not_id = "GoCom_notifications";
    private final int service_id = 123;
    public Bitmap tempImg;
    private String TAG = "ComicService";
    private MangaListModel mangaList = new MangaListModel();

    private ArrayList<GalleryViewModel> galleryViewModels = new ArrayList<GalleryViewModel>();
    private DetailViewModel detailViewModel = new DetailViewModel();
    private ChapterViewModel chapterViewModel = new ChapterViewModel();

    private ArrayList<MangaDetailsModel> mangaDetailList = new ArrayList<MangaDetailsModel>();
    private ChapterDetailsModel chapterDetail = new ChapterDetailsModel();

    private HashMap<String, MangaInfoModel> mangaHash = new HashMap<String, MangaInfoModel>();
    private HashMap<String, MangaInfoModel> mangaHashByName = new HashMap<String, MangaInfoModel>();

    private MutableLiveData<ArrayList<GalleryViewModel>> observableGalleryList = new MutableLiveData<ArrayList<GalleryViewModel>>();
    private MutableLiveData<DetailViewModel> observableDetails = new MutableLiveData<DetailViewModel>();
    private MutableLiveData<ChapterViewModel> observableChapter = new MutableLiveData<ChapterViewModel>();

    public ComicsService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        runInForefround();
    }

    //Function for constructing Gallery Viewmodels
    public GalleryViewModel constructGalleryViewModel(MangaInfoModel rawmodel) {
        return ViewModelBuilder.buildGalleryViewModel(rawmodel);
    }

    public ArrayList<GalleryViewModel> getGalleryViewModels() {
        return galleryViewModels;
    }

    //Function updates the viewmodels in the gallery view with images
    public void updateViewModelsFromIndex(int start, int end) {
        for (int i = start; i < end + 1; i++) {
            getImageAndUpdateViewModel(mangaList.getMangas().get(i).getImageURI(), i);
        }
    }

    //Updates the Manga detail viewmodel
    public MutableLiveData<DetailViewModel> getMangaDetailViewmodel(String id) {

        getMangaDetails(id);
        getImageUpdateDetail(id);
        return observableDetails;
    }

    public MutableLiveData<ChapterViewModel> getMangaChapterViewModel(String chapterId) {
        getPagesUpdateViewModel(chapterId);
        return observableChapter;
    }

    //Function for getting the complete manga list
    public void getMangaList(final int start, final int end) {
        Log.d(TAG, "Making request");
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET, getString(R.string.mangaListURL), null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Gson gson = new GsonBuilder().create();
                        try {
                            mangaList = gson.fromJson(response.toString(), MangaListModel.class);

                            /*List<MangaInfoModel> mangaModels = mangaList.getMangas();
                            List<MangaInfoModel> toRemove = new ArrayList<>();

                            for (MangaInfoModel model :
                                    mangaModels) {
                                if (MangaValidator.getInstance().IsInBlacklist(model.getMangaID())) {
                                    toRemove.add(model);
                                }
                            }
                            mangaModels.removeAll(toRemove);
                            mangaList.setMangas(mangaModels);*/

                            List<MangaInfoModel> toRemove = new ArrayList<>();
                            for (MangaInfoModel model : mangaList.getMangas()) {
                                if(MangaValidator.getInstance().IsInBlacklist(model.getMangaID())){
                                    toRemove.add(model);
                                }
                                else {
                                    mangaHash.put(model.getMangaID(), model);
                                    mangaHashByName.put(model.getTitle().toLowerCase(), model);
                                    galleryViewModels.add(constructGalleryViewModel(model));
                                }
                            }
                            mangaList.getMangas().removeAll(toRemove);
                        } catch (Error e){
                        Log.d("Error", "Gson error: " + e.getMessage());
                    }
                        observableGalleryList.postValue(galleryViewModels);
                        updateViewModelsFromIndex(start, end);
                        Log.d(TAG, "getMangaList: Got manga list");
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Request failed");
            }
        });
        jsonRequest.setShouldCache(true);
        ApiSingleton.getInstance(this).addToRequestQueue(jsonRequest);
    }

    //Function for getting manga details
    public void getMangaDetails(final String mangaId) {
        Log.d(TAG, "getMangaDetails: Making request");

        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET,
                getString(R.string.mangaDetailURL) + mangaId + "/", null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        GsonBuilder gson = new GsonBuilder();
                        gson.registerTypeAdapter(ChapterModel.class, new ChapterDeserializer());
                        try {
                            MangaDetailsModel mangaDetailModel = gson.create().fromJson(response.toString(), MangaDetailsModel.class);
                            mangaDetailList.add(mangaDetailModel);
                            ViewModelBuilder.buildDetailViewModel(mangaDetailModel, detailViewModel);
                        } catch (Error e) {
                            Log.d("Error", "Gson error: " + e.getMessage());
                        }

                        observableDetails.postValue(detailViewModel);
                        Log.d(TAG, "Got manga detail with chapterId:" + mangaId);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Request failed");
            }
        });
        jsonRequest.setShouldCache(true);
        ApiSingleton.getInstance(this).addToRequestQueue(jsonRequest);
    }

    //Function for getting pages for a given chapter
    public void getPages(final String chapterId) {
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET,
                getString(R.string.pagesURL) + chapterId + "/", null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        GsonBuilder gson = new GsonBuilder();
                        gson.registerTypeAdapter(Page.class, new PageDeserializer());
                        try {
                            ChapterDetailsModel chapterDetailModel = gson.create().fromJson(response.toString(), ChapterDetailsModel.class);
                            chapterDetail = chapterDetailModel;
                        } catch (Error e) {
                            Log.d("Error", "Gson error: " + e.getMessage());
                        }
                        Log.d(TAG, "Got manga pages for chapter with id:" + chapterId);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Request failed");
                //Toast.makeText(getApplicationContext(), "failed", Toast.LENGTH_SHORT).show();
            }
        });
        jsonRequest.setShouldCache(true);
        ApiSingleton.getInstance(this).addToRequestQueue(jsonRequest);
    }

    //Function for getting a image from the API
    //Might be deprecated..... https://stackoverflow.com/questions/41104831/how-to-download-an-image-by-using-volley
    public void getImage(String imageId) {

        ImageRequest request = new ImageRequest(getString(R.string.imageURL) + imageId,
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        tempImg = bitmap;
                    }
                }, 0, 0, Bitmap.Config.RGB_565,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(getApplicationContext(),"failed",Toast.LENGTH_SHORT).show();
                    }
                });
        request.setShouldCache(true);
        ApiSingleton.getInstance(this).addToRequestQueue(request);
    }

    //Function for getting the pages and updating the viewmodel
    public void getPagesUpdateViewModel(final String chapterId) {
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET,
                getString(R.string.pagesURL) + chapterId + "/", null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        GsonBuilder gson = new GsonBuilder();
                        gson.registerTypeAdapter(Page.class, new PageDeserializer());
                        try {
                            chapterDetail = gson.create().fromJson(response.toString(), ChapterDetailsModel.class);
                        } catch (Error e) {
                            Log.d("Error", "Gson error: " + e.getMessage());
                        }
                        updateChapterDetailPageViewModels();
                        Log.d(TAG, "Got manga pages for chapter with chapterId:" + chapterId);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Request failed");
                //Toast.makeText(getApplicationContext(), "failed", Toast.LENGTH_SHORT).show();
            }
        });
        jsonRequest.setShouldCache(true);
        ApiSingleton.getInstance(this).addToRequestQueue(jsonRequest);
    }

    //Function for updating the chapterdetailpage viewmodel
    public void updateChapterDetailPageViewModels() {
        chapterViewModel.setPageImages(new ArrayList<PageViewModel>());
        int i = 0;
        for (Page page : chapterDetail.getImagesModels()) {
            PageViewModel tmp = new PageViewModel();
            tmp.setPage_number(page.getPage_number());
            chapterViewModel.getPageImages().add(tmp);
            getPageImages(page.getPage_image_id(), i);
            i++;
        }
        observableChapter.postValue(chapterViewModel);
    }

    //Function for getting the page images
    public void getPageImages(final String imageId, final int position) {
        ImageRequest request = new ImageRequest(getString(R.string.imageURL) + imageId,
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        if (position < chapterViewModel.getPageImages().size()) {
                            chapterViewModel.getPageImages().get(position).setPage_image_id(bitmap);
                            observableChapter.postValue(chapterViewModel);
                            Log.d(TAG, "getPageImages: " + imageId);
                        }
                    }
                }, 0, 0, Bitmap.Config.RGB_565,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(getApplicationContext(), "failed", Toast.LENGTH_SHORT).show();
                    }
                });
        request.setShouldCache(true);
        ApiSingleton.getInstance(this).addToRequestQueue(request);
    }

    //Function for getting images and updating the detailviewmodel
    public void getImageUpdateDetail(String imageId) {

        ImageRequest request = new ImageRequest(getString(R.string.imageURL) + mangaHash.get(imageId).getImageURI(),
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        detailViewModel.setFrontPage(bitmap);
                        observableDetails.postValue(detailViewModel);
                        //Toast.makeText(getApplicationContext(), "Got image", Toast.LENGTH_SHORT).show();
                    }
                }, 0, 0, Bitmap.Config.RGB_565,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(getApplicationContext(), "failed", Toast.LENGTH_SHORT).show();
                    }
                });
        request.setShouldCache(true);
        ApiSingleton.getInstance(this).addToRequestQueue(request);
    }

    //Function for getting the image and update the gallery viewmodel
    public void getImageAndUpdateViewModel(String imageId, final int position) {

        ImageRequest request = new ImageRequest(getString(R.string.imageURL) + imageId,
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        galleryViewModels.get(position).setFrontpage(bitmap);
                        galleryViewModels.get(position).setImage_width(bitmap.getWidth());
                        galleryViewModels.get(position).setImage_height(bitmap.getHeight());
                        observableGalleryList.postValue(galleryViewModels);
                    }
                }, 0, 0, Bitmap.Config.RGB_565,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        //Toast.makeText(getApplicationContext(), "failed", Toast.LENGTH_SHORT).show();
                    }
                });
        request.setShouldCache(true);
        ApiSingleton.getInstance(this).addToRequestQueue(request);
    }

    public MutableLiveData<ArrayList<GalleryViewModel>> getObservableGalleryList() {
        return observableGalleryList;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    //Function for running the service in foreground
    //REF: https://developer.android.com/training/notify-user/build-notification
    private void runInForefround() {
        if (Build.VERSION.SDK_INT >= 26) {

            CharSequence name = getText(R.string.app_name);
            String description = getText(R.string.app_name).toString();
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(not_id, name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);

            Intent notificationIntent = new Intent(this, MainActivity.class);
            PendingIntent pendingIntent =
                    PendingIntent.getActivity(this, 0, notificationIntent, 0);

            Notification notification =
                    new Notification.Builder(this, not_id)
                            .setContentTitle(getString(R.string.app_name))
                            .setContentText(getText(R.string.gocomic_notification_discription))
                            .setSmallIcon(R.drawable.gocomicicon)
                            .setContentIntent(pendingIntent)
                            .build();
            startForeground(service_id, notification);
        } else {
            Intent notificationIntent = new Intent(this, MainActivity.class);
            PendingIntent pendingIntent =
                    PendingIntent.getActivity(this, 0, notificationIntent, 0);

            Notification notification =
                    new NotificationCompat.Builder(this, not_id)
                            .setContentTitle(getString(R.string.app_name))
                            .setContentText(getText(R.string.gocomic_notification_discription))
                            .setSmallIcon(R.drawable.gocomicicon)
                            .setContentIntent(pendingIntent)
                            .build();

            startForeground(service_id, notification);
        }
    }

    public DetailViewModel getDetailViewModel() {
        return this.detailViewModel;
    }

    public MutableLiveData<ChapterViewModel> getObservableChapter() {
        return observableChapter;
    }

    //Binding setup
    public class ComicsServiceBinder extends Binder {
        public ComicsService getService() {
            return ComicsService.this;
        }
    }
}
