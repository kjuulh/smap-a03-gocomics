package com.awesomenestgroup.gocomics.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.NonNull;

import com.awesomenestgroup.gocomics.listeners.OnCompleteRequestListener;
import com.awesomenestgroup.gocomics.providers.UserProvider;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;

public class UsersService extends Service {

    private final IBinder binder = new UsersServiceBinder();
    private String TAG = "UsersService";
    private FirebaseAuth mAuth;
    private GoogleSignInClient mGoogleSignInClient;

    public UsersService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind: UsersService bound");

        initializeService();
        return binder;
    }

    private void initializeService() {
        mAuth = FirebaseAuth.getInstance();
    }

    public void registerUserWithEmailAndPassword(final OnCompleteRequestListener listener, String email, String password) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "registerUserWithEmailAndPassword: success");
                        } else {
                            Log.w(TAG, "registerUserWithEmailAndPassword: failed", task.getException());
                        }
                        listener.getResult(UserProvider.getInstance().isLoggedIn());
                    }
                });
    }

    public void signInWithEmailAndPassword(final OnCompleteRequestListener listener, String email, String password) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithEmailAndPassword: success");
                        } else {
                            Log.w(TAG, "signInWithEmailAndPassword: failed", task.getException());
                        }
                        listener.getResult(UserProvider.getInstance().isLoggedIn());
                    }
                });
    }

    public void firebaseAuthWithGoogle(final OnCompleteRequestListener listener, GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithCredentials: success");
                            listener.getResult(true);
                        } else {
                            Log.w(TAG, "signInWithCredentials: failure", task.getException());
                            listener.getResult(false);
                        }
                    }
                });
    }

    public void signOut(final OnCompleteRequestListener listener) {
        mAuth.signOut();
        if (UserProvider.getInstance().isLoggedIn()) {
            listener.getResult(false);
        }
        listener.getResult(true);
    }

    //Binding setup
    public class UsersServiceBinder extends Binder {
        public UsersService getService() {
            return UsersService.this;
        }
    }
}
