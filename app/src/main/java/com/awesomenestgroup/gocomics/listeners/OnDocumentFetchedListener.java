package com.awesomenestgroup.gocomics.listeners;

public interface OnDocumentFetchedListener {
    void getResults(Object object);
}

