package com.awesomenestgroup.gocomics.listeners;

public interface OnCompleteRequestListener {
    void getResult(Boolean success);
}
