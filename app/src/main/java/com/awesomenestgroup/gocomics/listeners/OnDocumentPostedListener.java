package com.awesomenestgroup.gocomics.listeners;

public interface OnDocumentPostedListener {
    void getResults(Boolean success);
}
