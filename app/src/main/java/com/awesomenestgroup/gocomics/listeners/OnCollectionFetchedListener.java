package com.awesomenestgroup.gocomics.listeners;

public interface OnCollectionFetchedListener {
    void getResults(Object object);
}

