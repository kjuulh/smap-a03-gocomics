package com.awesomenestgroup.gocomics.listeners;

public interface OnDocumentExistsListener {
    void getResults(Boolean success);
}
