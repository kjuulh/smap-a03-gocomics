package com.awesomenestgroup.gocomics.listeners;

public interface OnDocumentRemovedListener {
    void getResults(Boolean success);
}
