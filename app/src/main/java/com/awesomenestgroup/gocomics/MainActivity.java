package com.awesomenestgroup.gocomics;


import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import com.android.volley.RequestQueue;
import com.awesomenestgroup.gocomics.providers.UserProvider;
import com.awesomenestgroup.gocomics.services.ComicsService;
import com.awesomenestgroup.gocomics.services.UsersService;
import com.awesomenestgroup.gocomics.utility.ApiSingleton;
import com.awesomenestgroup.gocomics.validator.MangaValidator;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private ComicsService comicsService;
    private UsersService usersService;
    private ServiceConnection comicsServiceConnection;
    private ServiceConnection usersServiceConnection;
    private boolean comicServiceBound = false;
    private boolean usersServiceBound = false;
    private NavigationView navigationView;
    private FirebaseAuth mAuth;
    private TextView user_email;

    private String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAuth = FirebaseAuth.getInstance();
        MangaValidator.getInstance(); // kickoff query early, to offset race condition

        //startComicService();
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.app_name));
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorBackgroundDark));
        setSupportActionBar(toolbar);

        //Here we control our burger bar, where we open and close it.
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);


        refreshLogin();
    }

    public void refreshLogin() {
        View headerv = navigationView.getHeaderView(0);
        user_email = headerv.findViewById(R.id.user_email_view);

        // shows differents navigation drawer. if the user is logged in or not.
        if (UserProvider.getInstance().isLoggedIn()) {
            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.activity_main_drawer);
            user_email.setText(UserProvider.getInstance().getUser().getEmail());

        } else {

            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.login_default_drawer);
            user_email.setText("");

        }
    }

    private void startComicService() {
        Intent backgroundServiceIntent = new Intent(MainActivity.this, ComicsService.class);
        startService(backgroundServiceIntent);
        setupConnectionToComicsService();
        bindToComicService();
        RequestQueue queue = ApiSingleton.getInstance(this.getApplicationContext()).getRequestQueue();
    }

    private void startUsersService() {
        Intent backgroundServiceIntent = new Intent(MainActivity.this, UsersService.class);
        startService(backgroundServiceIntent);
        setupConnectionToUsersService();
        bindToUsersService();
    }

    private void setupConnectionToComicsService() {
        comicsServiceConnection = new ServiceConnection() {
            public void onServiceConnected(ComponentName className, IBinder service) {
                comicsService = ((ComicsService.ComicsServiceBinder) service).getService();
                Log.d(TAG, "onServiceConnected: ComicService Connected");
                comicsService.getMangaList(0, 20);
                comicsService.getMangaDetails(getString(R.string.mangaedenid));
                comicsService.getImage(getString(R.string.mangaedenimageid));
                comicsService.getPages(getString(R.string.mangaedenchapterid));
            }

            public void onServiceDisconnected(ComponentName className) {
                comicsService = null;
                Log.d(TAG, "onServiceDisconnected: ComicService disconnected");

            }
        };
    }

    private void setupConnectionToUsersService() {
        usersServiceConnection = new ServiceConnection() {
            public void onServiceConnected(ComponentName className, IBinder service) {
                usersService = ((UsersService.UsersServiceBinder) service).getService();
                Log.d(TAG, "onServiceConnected: UsersService connected");
            }

            public void onServiceDisconnected(ComponentName className) {
                usersService = null;
                Log.d(TAG, "onServiceDisconnected: UsersService disconnected");

            }
        };
    }

    void bindToComicService() {
        bindService(new Intent(MainActivity.this, ComicsService.class),
                comicsServiceConnection, Context.BIND_AUTO_CREATE);
        comicServiceBound = true;
    }

    void bindToUsersService() {
        bindService(new Intent(MainActivity.this, UsersService.class),
                usersServiceConnection, Context.BIND_AUTO_CREATE);
        usersServiceBound = true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.action_bar_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        int id = item.getItemId();
        NavHostFragment host = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.my_nav_host_fragment);
        NavController navController = host.getNavController();

        //noinspection SimplifiableIfStatement
        if (id == R.id.manga_search) {
            navController.navigate(R.id.search);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        NavHostFragment host = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.my_nav_host_fragment);
        NavController navController = host.getNavController();

        switch (id) {
            case R.id.nav_home:
                navController.navigate(R.id.gallery);
                break;
            case R.id.nav_booksmarks:
                navController.navigate(R.id.bookmarksFragment);
                break;
            case R.id.nav_Recents:
                navController.navigate(R.id.recentsFragment);
                break;
            case R.id.nav_profile:
                navController.navigate(R.id.profileFragment);
                break;
            case R.id.nav_settings:
                navController.navigate(R.id.settingsFragment);
                break;
            case R.id.nav_about:
                navController.navigate(R.id.aboutFragment);
                break;
            case R.id.nav_logout:
                mAuth.signOut();
                refreshLogin();
                break;
            case R.id.nav_login:
                navController.navigate(R.id.login);
                refreshLogin();
                break;
            case R.id.nav_sign_up:
                navController.navigate(R.id.signUp);
                refreshLogin();
                break;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
