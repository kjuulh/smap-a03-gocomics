package com.awesomenestgroup.gocomics.fragments;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.awesomenestgroup.gocomics.R;
import com.awesomenestgroup.gocomics.adapters.PagesRecyclerViewAdapter;
import com.awesomenestgroup.gocomics.listeners.OnDocumentPostedListener;
import com.awesomenestgroup.gocomics.models.Comic;
import com.awesomenestgroup.gocomics.providers.UserProvider;
import com.awesomenestgroup.gocomics.repositories.BookmarksRepository;
import com.awesomenestgroup.gocomics.services.ComicsService;
import com.awesomenestgroup.gocomics.viewModels.models.ChapterViewModel;
import com.awesomenestgroup.gocomics.viewModels.models.PageViewModel;

import java.util.ArrayList;


public class ReadingFragment extends Fragment {
    private String chapterId;
    private ServiceConnection comicsServiceConnection;
    private boolean bound = false;
    private ComicsService comicsService;
    private PagesRecyclerViewAdapter PRVA;
    private String comicId;
    private String chapterTitle;
    private String TAG = "ReadingFragment";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            chapterId = getArguments().getString("chapterId");
            chapterTitle = getArguments().getString("chapterTitle");
            comicId = getArguments().getString("comicId");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_reading, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initializeView();
    }

    private void initializeView() {
        LinearLayoutManager manager = new LinearLayoutManager(getView().getContext());
        manager.setReverseLayout(true);
        manager.setStackFromEnd(true);


        RecyclerView recyclerView = getView().findViewById(R.id.recycler_reading_pages);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(manager);
        PRVA = new PagesRecyclerViewAdapter(getContext(), new ArrayList<PageViewModel>());
        recyclerView.setAdapter(PRVA);
        startService();

        if (UserProvider.getInstance().isLoggedIn()) {
            BookmarksRepository.getInstance().updateLatestChapterRead(comicId, new Comic.ChapterKeyPair(chapterId, chapterTitle), new OnDocumentPostedListener() {
                @Override
                public void getResults(Boolean success) {
                    if (success) {
                        Log.d(TAG, "updateLatestChapterRead: Bookmark update");
                    } else {
                        Log.w(TAG, "updateLatestChapterRead: failure");
                    }
                }
            });
        }

    }

    private void setRecycler() {
        comicsService.getMangaChapterViewModel(chapterId).observe(this, new Observer<ChapterViewModel>() {
            @Override
            public void onChanged(ChapterViewModel model) {
                PRVA.setData(model.getPageImages());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    public void onStop() {
        super.onStop();
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    //Service binding
    private void startService() {
        setupConnectionToComicsService();
        bindToMovieService();
    }

    void bindToMovieService() {
        getActivity().bindService(new Intent(getActivity(),
                ComicsService.class), comicsServiceConnection, Context.BIND_AUTO_CREATE);
        bound = true;
    }

    private void setupConnectionToComicsService() {
        comicsServiceConnection = new ServiceConnection() {
            public void onServiceConnected(ComponentName className, IBinder service) {
                comicsService = ((ComicsService.ComicsServiceBinder) service).getService();
                setRecycler();
                Log.d("Log", "Comic service connected in reading fragment");
            }

            public void onServiceDisconnected(ComponentName className) {
                comicsService = null;
                Log.d("LOG", "Comic service disconnected");

            }
        };
    }
}
