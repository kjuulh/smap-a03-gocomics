package com.awesomenestgroup.gocomics.fragments;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.awesomenestgroup.gocomics.R;
import com.awesomenestgroup.gocomics.adapters.GalleryRecyclerViewAdapter;
import com.awesomenestgroup.gocomics.services.ComicsService;
import com.awesomenestgroup.gocomics.viewModels.models.GalleryViewModel;

import java.util.ArrayList;


public class SearchFragment extends Fragment {

    private GridLayoutManager gridLayoutManager;
    private EditText comicTitleEditText;
    private GalleryRecyclerViewAdapter galleryRecyclerViewAdapter;
    private ServiceConnection comicsServiceConnection;
    private boolean bound = false;
    private ComicsService comicsService;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstance) {
        super.onViewCreated(view, savedInstance);
        gridLayoutManager = new GridLayoutManager(getView().getContext(), 3);
        startService();
        initializeView();
        setListeners();
    }

    private void startService() {
        setupConnectionToComicsService();
        bindToComicService();
    }

    private void setupConnectionToComicsService() {
        comicsServiceConnection = new ServiceConnection() {
            public void onServiceConnected(ComponentName className, IBinder service) {
                comicsService = ((ComicsService.ComicsServiceBinder) service).getService();
                Log.d("Log", "Comic service connected");

                setRecycler();
            }

            public void onServiceDisconnected(ComponentName className) {
                comicsService = null;
                Log.d("LOG", "Movie service disconnected");

            }
        };
    }

    void bindToComicService() {
        getActivity().bindService(new Intent(getActivity(),
                ComicsService.class), comicsServiceConnection, Context.BIND_AUTO_CREATE);
        bound = true;

    }

    private void setRecycler() {
        comicsService.getObservableGalleryList().observe(this, new Observer<ArrayList<GalleryViewModel>>() {
            @Override
            public void onChanged(ArrayList<GalleryViewModel> galleryViewModels) {
                galleryRecyclerViewAdapter.setData(galleryViewModels);
            }
        });
    }

    private void initializeView() {

        RecyclerView recyclerView = getView().findViewById(R.id.recyclerView_search_comics);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(gridLayoutManager);

        galleryRecyclerViewAdapter = new GalleryRecyclerViewAdapter(getContext(), new ArrayList<GalleryViewModel>());
        recyclerView.setAdapter(galleryRecyclerViewAdapter);

        comicTitleEditText = getView().findViewById(R.id.editText_search_title);
    }

    private void setListeners() {
        comicTitleEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(final Editable s) {
                comicsService.getObservableGalleryList().observe(SearchFragment.this, new Observer<ArrayList<GalleryViewModel>>() {
                    @Override
                    public void onChanged(ArrayList<GalleryViewModel> galleryViewModels) {
                        ArrayList<GalleryViewModel> galleryViewModelArrayList = new ArrayList<>();

                        for (GalleryViewModel galleryView : galleryViewModels) {

                            if (galleryView.getTitle().toLowerCase().contains(s.toString().toLowerCase())) {
                                galleryViewModelArrayList.add(galleryView);
                            }

                        }

                        galleryRecyclerViewAdapter.setData(galleryViewModelArrayList);
                    }
                });
            }
        });
    }
}
