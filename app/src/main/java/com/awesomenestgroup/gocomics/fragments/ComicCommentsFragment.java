package com.awesomenestgroup.gocomics.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.awesomenestgroup.gocomics.R;
import com.awesomenestgroup.gocomics.adapters.CommentsRecyclerViewAdapter;
import com.awesomenestgroup.gocomics.listeners.OnCollectionFetchedListener;
import com.awesomenestgroup.gocomics.listeners.OnDocumentPostedListener;
import com.awesomenestgroup.gocomics.models.Comment;
import com.awesomenestgroup.gocomics.providers.UserProvider;
import com.awesomenestgroup.gocomics.repositories.CommentsRepository;
import com.awesomenestgroup.gocomics.viewModels.CommentsViewModel;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

public class ComicCommentsFragment extends Fragment {

    //TODO: Add comicId

    private String TAG = "ComicCommentsFragment";

    private CommentsViewModel commentsViewModel;
    private EditText commentEditText;
    private Button commentButton;
    private String comicId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_comic_comments, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        comicId = ((DetailsFragment) getParentFragment()).getComicId();
        initializeView();
        setListeners();
    }

    private void initializeView() {

        commentEditText = getView().findViewById(R.id.editText_comments);
        commentButton = getView().findViewById(R.id.button_comments_comment);

        LinearLayoutManager manager = new LinearLayoutManager(getView().getContext());

        RecyclerView recyclerView = getView().findViewById(R.id.recycler_recents);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(manager);
        final CommentsRecyclerViewAdapter CRVA = new CommentsRecyclerViewAdapter(getContext(), new ArrayList<Comment>());
        recyclerView.setAdapter(CRVA);


        CommentsRepository.getInstance().observeComments(comicId, new OnCollectionFetchedListener() {
            @Override
            public void getResults(Object object) {
                CRVA.setData((List<Comment>) object);
            }
        });
    }

    private void setListeners() {
        commentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleUserComment(commentEditText.getText().toString());
            }
        });
    }

    private void handleUserComment(String comment) {
        class AddCommentTask extends AsyncTask<String, Void, Void> {

            @Override
            protected Void doInBackground(String... strings) {
                CommentsRepository.getInstance().addComment(comicId, strings[0], new OnDocumentPostedListener() {
                    @Override
                    public void getResults(Boolean success) {
                        if (success) {
                            Snackbar.make(getView(), getContext().getString(R.string.comiccommentsfragment_comment_added), Snackbar.LENGTH_SHORT);
                            Log.d(TAG, "handleUserComment: success");
                        } else {
                            Log.w(TAG, "handleUserComment: failure");
                        }
                    }
                }); //TODO: Add comicId
                return null;
            }
        }

        UserProvider userProvider = UserProvider.getInstance();

        if (userProvider.isLoggedIn()) {
            new AddCommentTask().execute(comment);
        } else {
            Snackbar.make(getView(), getContext().getString(R.string.comiccommentsfragment_login), Snackbar.LENGTH_LONG);
        }
    }
}
