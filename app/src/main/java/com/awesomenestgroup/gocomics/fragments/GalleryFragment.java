package com.awesomenestgroup.gocomics.fragments;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.RequestQueue;
import com.awesomenestgroup.gocomics.R;
import com.awesomenestgroup.gocomics.adapters.GalleryRecyclerViewAdapter;
import com.awesomenestgroup.gocomics.services.ComicsService;
import com.awesomenestgroup.gocomics.services.UsersService;
import com.awesomenestgroup.gocomics.utility.ApiSingleton;
import com.awesomenestgroup.gocomics.viewModels.models.GalleryViewModel;

import java.util.ArrayList;


public class GalleryFragment extends Fragment {

    private ComicsService comicsService;
    private ServiceConnection comicsServiceConnection;
    private boolean comicServiceBound = false;

    private UsersService usersService;
    private ServiceConnection usersServiceConnection;
    private boolean usersServiceBound = false;

    private GridLayoutManager gridLayoutManager;
    private Button searchButton;
    private Button bookmarksButton;
    private Button recentsButton;

    private GalleryRecyclerViewAdapter galleryRecyclerViewAdapter;
    private RecyclerView recyclerView;

    private String TAG = "GalleryFragment";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startComicService();
        startUsersService();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_gallery, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstance) {
        super.onViewCreated(view, savedInstance);
        gridLayoutManager = new GridLayoutManager(getView().getContext(), 3);

        initializeView();
        setListeners();
        if (comicsService != null)
            setRecycler();
    }

    private void initializeView() {
        recyclerView = getView().findViewById(R.id.recyclerView_gallery_comics);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(gridLayoutManager);

        galleryRecyclerViewAdapter = new GalleryRecyclerViewAdapter(getContext(), new ArrayList<GalleryViewModel>());
        recyclerView.setAdapter(galleryRecyclerViewAdapter);

        searchButton = getView().findViewById(R.id.button_gallery_search);
        bookmarksButton = getView().findViewById(R.id.button_gallery_bookmarks);
        recentsButton = getView().findViewById(R.id.button_gallery_recents);
    }

    private void setListeners() {
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(getView()).navigate(R.id.action_gallery_to_search);
            }
        });

        bookmarksButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(getView()).navigate(R.id.action_gallery_to_bookmarks);
            }
        });

        recentsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(getView()).navigate(R.id.action_gallery_to_recents);
            }
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    int position = gridLayoutManager.findFirstVisibleItemPosition();
                    if (position < 10) {
                        comicsService.getMangaList(0, 10);
                    } else {
                        comicsService.updateViewModelsFromIndex(position, position + 20);
                    }
                }
            }
        });
    }

    private void startComicService() {
        Intent backgroundServiceIntent = new Intent(getActivity(), ComicsService.class);
        getActivity().startService(backgroundServiceIntent);
        setupConnectionToComicsService();
        bindToMovieService();
        RequestQueue queue = ApiSingleton.getInstance(this.getActivity().getApplicationContext()).getRequestQueue();
    }

    private void startUsersService() {
        Intent backgroundServiceIntent = new Intent(getActivity(), UsersService.class);
        getActivity().startService(backgroundServiceIntent);
        setupConnectionToUsersService();
        bindToUsersService();
    }

    private void setupConnectionToComicsService() {
        comicsServiceConnection = new ServiceConnection() {
            public void onServiceConnected(ComponentName className, IBinder service) {
                comicsService = ((ComicsService.ComicsServiceBinder) service).getService();
                Log.d(TAG, "onServiceConnected: ComicService connected");
                comicsService.getMangaList(0, 20);
                comicsService.getMangaDetails("5bfdd0ff719a162b3c196677");
                comicsService.getImage("4e/4e55aeda6ba2044eb2762124688b61e74f24880515e71827f1f1e2c4.png");
                comicsService.getPages("5bfe47b5719a167a5c3e8224");
                setRecycler();
            }

            public void onServiceDisconnected(ComponentName className) {
                comicsService = null;
                Log.d(TAG, "onServiceConnected: MovieService disconnected");

            }
        };
    }

    private void setupConnectionToUsersService() {
        usersServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                usersService = ((UsersService.UsersServiceBinder) service).getService();
                Log.d(TAG, "onServiceConnected: UsersService connected");
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                Log.d(TAG, "onServiceDisconnected: UsersService disconnected");
            }
        };
    }


    void bindToMovieService() {
        getActivity().bindService(new Intent(getActivity(), ComicsService.class),
                comicsServiceConnection, Context.BIND_AUTO_CREATE);
        comicServiceBound = true;
    }

    void bindToUsersService() {
        getActivity().bindService(new Intent(getActivity(), UsersService.class),
                usersServiceConnection, Context.BIND_AUTO_CREATE);
        usersServiceBound = true;
    }

    private void setRecycler() {
        comicsService.getObservableGalleryList().observe(this, new Observer<ArrayList<GalleryViewModel>>() {
            @Override
            public void onChanged(ArrayList<GalleryViewModel> galleryViewModels) {
                galleryRecyclerViewAdapter.setData(galleryViewModels);
            }
        });
    }

}
