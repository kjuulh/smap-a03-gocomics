package com.awesomenestgroup.gocomics.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.awesomenestgroup.gocomics.R;
import com.awesomenestgroup.gocomics.adapters.RecentsRecyclerViewAdapter;
import com.awesomenestgroup.gocomics.models.Recent;

import java.util.ArrayList;
import java.util.List;

public class RecentsFragment extends Fragment {

    private LinearLayoutManager linearLayoutManager;
    private RecyclerView recyclerView;
    private RecentsRecyclerViewAdapter recentsRecyclerViewAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_recents, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstance) {
        super.onViewCreated(view, savedInstance);
        linearLayoutManager = new LinearLayoutManager(getView().getContext());

        initializeView();
    }

    private void initializeView() {
        recyclerView = getView().findViewById(R.id.recycler_recents);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);

        List<Recent> mockedRecents = new ArrayList<Recent>();

        Recent bm1 = new Recent();
        bm1.setTitle("Comic - 1");
        mockedRecents.add(bm1);

        Recent bm2 = new Recent();
        bm2.setTitle("Comic - 2");
        mockedRecents.add(bm2);
        mockedRecents.add(bm2);
        mockedRecents.add(bm2);
        mockedRecents.add(bm2);
        mockedRecents.add(bm2);
        mockedRecents.add(bm2);
        mockedRecents.add(bm2);

        recentsRecyclerViewAdapter = new RecentsRecyclerViewAdapter(getContext(), mockedRecents);
        recyclerView.setAdapter(recentsRecyclerViewAdapter);
    }
}
