package com.awesomenestgroup.gocomics.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.awesomenestgroup.gocomics.R;
import com.awesomenestgroup.gocomics.providers.UserProvider;
import com.google.firebase.auth.FirebaseUser;


public class ProfileFragment extends Fragment {

    private TextView emailText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initializeView();
    }

    private void initializeView() {
        emailText = getView().findViewById(R.id.text_profile_email);
        if (UserProvider.getInstance().isLoggedIn()) {
            FirebaseUser user = UserProvider.getInstance().getUser();
            emailText.setText(user.getEmail());
        } else {
            emailText.setText("N/A");
        }

    }
}
