package com.awesomenestgroup.gocomics.fragments;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.awesomenestgroup.gocomics.R;
import com.awesomenestgroup.gocomics.listeners.OnCompleteRequestListener;
import com.awesomenestgroup.gocomics.providers.UserProvider;
import com.awesomenestgroup.gocomics.services.UsersService;
import com.awesomenestgroup.gocomics.validator.EmailValidator;
import com.google.firebase.auth.FirebaseUser;


public class SignUpFragment extends Fragment {

    private String TAG = "SignUpFragment";

    private EditText emailText;
    private EditText passwordText;
    private EditText confirmPasswordText;
    private Button registerButton;
    private Button cancelButton;

    private UsersService usersService;
    private ServiceConnection usersServiceConnection;
    private boolean usersServiceBound = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sign_up, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        startUsersService();
        initializeView();
        setListeners();
    }

    private void initializeView() {
        emailText = getView().findViewById(R.id.edit_signup_email);
        passwordText = getView().findViewById(R.id.edit_signup_password);
        confirmPasswordText = getView().findViewById(R.id.edit_signup_confirmpassword);
        registerButton = getView().findViewById(R.id.button_signup_register);
        cancelButton = getView().findViewById(R.id.button_signup_cancel);
    }

    private void setListeners() {

        if (!emailText.getText().toString().equals("") ||
                !passwordText.getText().toString().equals("") ||
                !confirmPasswordText.getText().toString().equals("")) {
            return;
        }

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                String email = emailText.getText().toString();
                if (!EmailValidator.isEmailValid(email)) {
                    Toast.makeText(getContext(), getContext().getString(R.string.signupfragment_invalid_email), Toast.LENGTH_SHORT).show();
                    return;
                }
                String password = passwordText.getText().toString();
                if (!password.equals(confirmPasswordText.getText().toString()) || password.isEmpty() || password.length() < 8) {
                    Toast.makeText(getContext(), getContext().getString(R.string.signupfragment_password_doesnt_match), Toast.LENGTH_SHORT).show();
                    return;
                }

                usersService.registerUserWithEmailAndPassword(new OnCompleteRequestListener() {
                    @Override
                    public void getResult(Boolean success) {
                        if (success) {
                            Toast.makeText(v.getContext(), getString(R.string.signupfragment_user_registered), Toast.LENGTH_SHORT).show();
                            Navigation.findNavController(v).navigate(R.id.action_signUp_to_login);
                        } else {
                            Toast.makeText(v.getContext(), getContext().getString(R.string.signupfragment_registration_failed), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, email, password);
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).popBackStack();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();

        // Check if user is signed in
        FirebaseUser currentUser = UserProvider.getInstance().getUser();
    }

    void bindToUsersService() {
        getActivity().bindService(new Intent(getActivity(), UsersService.class),
                usersServiceConnection, Context.BIND_AUTO_CREATE);
        usersServiceBound = true;
    }

    private void startUsersService() {
        Intent backgroundServiceIntent = new Intent(getActivity(), UsersService.class);
        getActivity().startService(backgroundServiceIntent);
        setupConnectionToUsersService();
        bindToUsersService();
    }

    private void setupConnectionToUsersService() {
        usersServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                usersService = ((UsersService.UsersServiceBinder) service).getService();
                Log.d(TAG, "onServiceConnected: UsersService connected");
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                Log.d(TAG, "onServiceDisconnected: UsersService disconnected");
            }
        };
    }
}
