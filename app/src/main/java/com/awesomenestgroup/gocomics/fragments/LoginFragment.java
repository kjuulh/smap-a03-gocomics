package com.awesomenestgroup.gocomics.fragments;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.awesomenestgroup.gocomics.MainActivity;
import com.awesomenestgroup.gocomics.R;
import com.awesomenestgroup.gocomics.listeners.OnCompleteRequestListener;
import com.awesomenestgroup.gocomics.providers.UserProvider;
import com.awesomenestgroup.gocomics.services.UsersService;
import com.awesomenestgroup.gocomics.validator.EmailValidator;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;


public class LoginFragment extends Fragment {

    private static final int RC_SIGN_IN = 123;

    private EditText emailText;
    private EditText passwordText;
    private Button loginButton;
    private Button cancelButton;
    private String TAG = "LoginFragment";
    private SignInButton signInButton;
    private GoogleSignInClient mGoogleSignInClient;

    private UsersService usersService;
    private ServiceConnection usersServiceConnection;
    private boolean usersServiceBound = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(getContext(), gso);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        startUsersService();
        initializeView();
        setListeners();
    }

    private void initializeView() {
        emailText = getView().findViewById(R.id.edit_login_email);
        passwordText = getView().findViewById(R.id.edit_login_password);
        loginButton = getView().findViewById(R.id.button_login_login);
        cancelButton = getView().findViewById(R.id.button_login_cancel);
        signInButton = getView().findViewById(R.id.button_login_google_login);
        signInButton.setSize(SignInButton.SIZE_STANDARD);
    }

    private void setListeners() {
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (emailText.getText().toString().equals("") && passwordText.getText().toString().equals("")) {
                    return;
                }
                handleLogin(new OnCompleteRequestListener() {
                    @Override
                    public void getResult(Boolean success) {
                        if (success) {
                            Snackbar.make(getView(), getContext().getString(R.string.login_fragment_authentication_success), Snackbar.LENGTH_SHORT).show();
                            Navigation.findNavController(v).popBackStack(R.id.gallery, false);
                            MainActivity mainActivity = (MainActivity) getActivity();
                            mainActivity.refreshLogin();
                        }
                    }
                });
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(v).popBackStack(R.id.gallery, false);
            }
        });

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.button_login_google_login:
                        signIn();
                        break;
                }
            }
        });
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                usersService.firebaseAuthWithGoogle(new OnCompleteRequestListener() {
                    @Override
                    public void getResult(Boolean success) {
                        if (success) {
                            Navigation.findNavController(getView()).popBackStack(R.id.gallery, false);
                            MainActivity mainActivity = (MainActivity) getActivity();
                            mainActivity.refreshLogin();

                            Snackbar.make(getView().findViewById(R.id.constraint_login), getContext().getString(R.string.login_fragment_authentication_success), Snackbar.LENGTH_SHORT).show();
                        } else {
                            Snackbar.make(getView().findViewById(R.id.constraint_login), getContext().getString(R.string.login_fragment_authentication_failed), Snackbar.LENGTH_SHORT).show();
                        }
                    }
                }, account);
            } catch (ApiException e) {
                Log.w(TAG, "Google sign in failed", e);
            }
        }
    }

    private void handleLogin(final OnCompleteRequestListener onCompleteRequestListener) {

        String email = emailText.getText().toString();
        if (!EmailValidator.isEmailValid(email)) {
            Toast.makeText(getContext(), getContext().getString(R.string.loginfragment_invalidemail), Toast.LENGTH_SHORT).show();
            onCompleteRequestListener.getResult(false);
            return;
        }
        String password = passwordText.getText().toString();
        if (password.isEmpty()) {
            Toast.makeText(getContext(), getContext().getString(R.string.loginfragment_password_invalid), Toast.LENGTH_SHORT).show();
            onCompleteRequestListener.getResult(false);
            return;
        }

        usersService.signInWithEmailAndPassword(new OnCompleteRequestListener() {
            @Override
            public void getResult(Boolean success) {
                if (success) {
                    Snackbar.make(getView().findViewById(R.id.constraint_login), getContext().getString(R.string.login_fragment_authentication_success), Snackbar.LENGTH_SHORT).show();
                } else {
                    Snackbar.make(getView().findViewById(R.id.constraint_login), getContext().getString(R.string.login_fragment_authentication_failed), Snackbar.LENGTH_SHORT).show();
                }
                onCompleteRequestListener.getResult(UserProvider.getInstance().isLoggedIn());
            }
        }, email, password);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    void bindToUsersService() {
        getActivity().bindService(new Intent(getActivity(), UsersService.class),
                usersServiceConnection, Context.BIND_AUTO_CREATE);
        usersServiceBound = true;
    }

    private void startUsersService() {
        Intent backgroundServiceIntent = new Intent(getActivity(), UsersService.class);
        getActivity().startService(backgroundServiceIntent);
        setupConnectionToUsersService();
        bindToUsersService();
    }

    private void setupConnectionToUsersService() {
        usersServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                usersService = ((UsersService.UsersServiceBinder) service).getService();
                Log.d(TAG, "onServiceConnected: UsersService connected");
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                Log.d(TAG, "onServiceDisconnected: UsersService disconnected");
            }
        };
    }

}
