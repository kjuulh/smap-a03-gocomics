package com.awesomenestgroup.gocomics.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.awesomenestgroup.gocomics.R;
import com.awesomenestgroup.gocomics.adapters.BookmarksRecyclerViewAdapter;
import com.awesomenestgroup.gocomics.listeners.OnCollectionFetchedListener;
import com.awesomenestgroup.gocomics.models.Bookmark;
import com.awesomenestgroup.gocomics.providers.UserProvider;
import com.awesomenestgroup.gocomics.repositories.BookmarksRepository;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

public class BookmarksFragment extends Fragment {

    private LinearLayoutManager linearLayoutManager;
    private RecyclerView recyclerView;
    private BookmarksRecyclerViewAdapter bookmarksRecyclerViewAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bookmarks, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstance) {
        super.onViewCreated(view, savedInstance);
        linearLayoutManager = new LinearLayoutManager(getView().getContext());

        initializeView();
        setListeners();
    }

    private void initializeView() {
        recyclerView = getView().findViewById(R.id.recycler_recents);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);

        bookmarksRecyclerViewAdapter = new BookmarksRecyclerViewAdapter(getContext(), new ArrayList<Bookmark>());
        recyclerView.setAdapter(bookmarksRecyclerViewAdapter);
    }

    private void setListeners() {
        if (UserProvider.getInstance().isLoggedIn()) {
            BookmarksRepository.getInstance().getBookmarks(new OnCollectionFetchedListener() {
                @Override
                public void getResults(Object object) {
                    List<Bookmark> bookmarks = (List<Bookmark>) object;

                    bookmarksRecyclerViewAdapter.setData(bookmarks);
                }
            });
        } else {
            Snackbar.make(getView(), getContext().getString(R.string.bookmarkfragment_login_to_bookmarks), Snackbar.LENGTH_LONG).show();
        }

    }
}
