package com.awesomenestgroup.gocomics.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.awesomenestgroup.gocomics.R;
import com.awesomenestgroup.gocomics.providers.UserProvider;
import com.awesomenestgroup.gocomics.repositories.NetworkRepository;
import com.awesomenestgroup.gocomics.repositories.NotificationsRepository;
import com.awesomenestgroup.gocomics.repositories.taskcallbacks.OnRepoTaskCompleted;
import com.google.android.material.snackbar.Snackbar;


public class SettingsFragment extends Fragment {
    private static final String TAG = "SettingsFragment";

    private TextView pagesToLoadCountText;
    private SeekBar pagesToLoadCountSeekBar;
    private Switch pullComicsSwitch;
    private Switch notificationsSwitch;
    private Button saveButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initializeView();
        setListeners();
    }

    private void initializeView() {
        pagesToLoadCountText = getView().findViewById(R.id.text_settings_pageLoaded_count);
        pagesToLoadCountSeekBar = getView().findViewById(R.id.seekBar_settings_pageLoaded);
        pullComicsSwitch = getView().findViewById(R.id.switch_settings_pull_comics);
        notificationsSwitch = getView().findViewById(R.id.switch_settings_notifications);
        saveButton = getView().findViewById(R.id.button_settings_save);

        if (UserProvider.getInstance().isLoggedIn()) {
            NetworkRepository.getInstance().getNetworkStatus("wifi", new OnRepoTaskCompleted() {
                @Override
                public void onNetworkStatusFetched(boolean status) {
                    super.onNetworkStatusFetched(status);

                    pullComicsSwitch.setChecked(status);
                }
            });

            NotificationsRepository.getInstance().getNotificationStatus(new OnRepoTaskCompleted() {
                @Override
                public void onNotificationStatusFetched(boolean status) {
                    super.onNotificationStatusFetched(status);

                    notificationsSwitch.setChecked(status);
                }
            });
        }
    }

    private void setListeners() {
        pagesToLoadCountSeekBar.setMax(20);
        pagesToLoadCountSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                pagesToLoadCountText.setText(String.format("" + progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        pagesToLoadCountSeekBar.setProgress(8);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleSave(v);
            }
        });
    }

    private void handleSave(View v) {

        if (UserProvider.getInstance().isLoggedIn()) {
            NetworkRepository.getInstance().addNetworkStatus("wifi", pullComicsSwitch.isChecked());
            if (notificationsSwitch.isChecked()) {
                NotificationsRepository.getInstance().enableNotifications();
            } else {
                NotificationsRepository.getInstance().disableNotifications();
            }
            Snackbar.make(getView().findViewById(R.id.constraint_settings), getContext().getString(R.string.settingsfragment_saving_changes), Snackbar.LENGTH_SHORT).show();
        } else {
            Snackbar.make(getView().findViewById(R.id.constraint_settings), getContext().getString(R.string.settingsfragment_login), Snackbar.LENGTH_SHORT).show();
        }
    }
}
