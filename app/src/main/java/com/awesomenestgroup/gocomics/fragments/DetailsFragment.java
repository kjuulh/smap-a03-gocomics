package com.awesomenestgroup.gocomics.fragments;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.awesomenestgroup.gocomics.R;
import com.awesomenestgroup.gocomics.adapters.ChaptersRecyclerViewAdapter;
import com.awesomenestgroup.gocomics.listeners.OnDocumentExistsListener;
import com.awesomenestgroup.gocomics.listeners.OnDocumentPostedListener;
import com.awesomenestgroup.gocomics.listeners.OnDocumentRemovedListener;
import com.awesomenestgroup.gocomics.models.ChapterModel;
import com.awesomenestgroup.gocomics.models.Comic;
import com.awesomenestgroup.gocomics.providers.UserProvider;
import com.awesomenestgroup.gocomics.repositories.BookmarksRepository;
import com.awesomenestgroup.gocomics.services.ComicsService;
import com.awesomenestgroup.gocomics.viewModels.models.DetailViewModel;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

public class DetailsFragment extends androidx.fragment.app.Fragment {

    private String TAG = "DetailsFragment";

    private ComicsService comicsService;
    private String comicId;
    private DetailViewModel mangaDetail;
    private ServiceConnection comicsServiceConnection;
    private boolean bound = false;
    private ChaptersRecyclerViewAdapter CRVA;
    private ImageView imageFront;
    private TextView title;
    private TextView genres;
    private TextView discription;
    private Button bookmarkButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            comicId = getArguments().getString("comicId");
        }
        startService();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeView();
        setListeners();
        if (comicsService != null)
            setRecycler();
    }

    private void initializeView() {
        imageFront = getView().findViewById(R.id.image_details_cover);
        genres = getView().findViewById(R.id.text_details_genre);
        title = getView().findViewById(R.id.text_details_title);
        discription = getView().findViewById(R.id.text_details_decription);
        bookmarkButton = getView().findViewById(R.id.button_details_bookmark);

        LinearLayoutManager manager = new LinearLayoutManager(getView().getContext());
        RecyclerView recyclerView = getView().findViewById(R.id.recycler_details_chapters);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(manager);
        CRVA = new ChaptersRecyclerViewAdapter(getContext(), new ArrayList<ChapterModel>(), comicId);
        recyclerView.setAdapter(CRVA);
    }

    private void setListeners() {
        bookmarkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (UserProvider.getInstance().isLoggedIn()) {
                    handleBookmarks();
                } else {
                    Snackbar.make(getView(), getContext().getString(R.string.detailsfragment_login_bookmarks), Snackbar.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void handleBookmarks() {
        final BookmarksRepository repository = BookmarksRepository.getInstance();

        repository.bookmarkExists(comicId, new OnDocumentExistsListener() {
            @Override
            public void getResults(Boolean success) {
                if (success) {

                    mangaDetail = comicsService.getDetailViewModel();

                    if (mangaDetail == null) {
                        return;
                    }

                    List<Comic.ChapterKeyPair> chapterKeyPairs = new ArrayList<>();

                    for (ChapterModel chapterModel :
                            mangaDetail.getChapters()) {
                        Comic.ChapterKeyPair comic = new Comic.ChapterKeyPair(chapterModel.getChapter_name(), chapterModel.getChapter_id());

                        chapterKeyPairs.add(comic);
                    }

                    Comic comic = new Comic(mangaDetail.getTitle(), comicId);
                    comic.setChapters(chapterKeyPairs);

                    repository.addBookmark(comic, new OnDocumentPostedListener() {
                        @Override
                        public void getResults(Boolean success) {
                            if (success) {
                                Snackbar.make(getView(), getContext().getString(R.string.detailsfragment_added_bookmark), Snackbar.LENGTH_SHORT).show();
                                Log.d(TAG, "addBookmark: success");
                            } else {
                                Log.d(TAG, "addBookmark: failure");
                            }
                        }
                    });
                } else {
                    repository.removeBookmark(comicId, new OnDocumentRemovedListener() {
                        @Override
                        public void getResults(Boolean success) {
                            if (success) {
                                Snackbar.make(getView(), getContext().getString(R.string.detailsfragment_removed_bookmarks), Snackbar.LENGTH_SHORT).show();
                                Log.d(TAG, "removeBookmark: success");
                            } else {
                                Log.d(TAG, "removeBookmark: failure");
                            }
                        }
                    });
                }
            }
        });
    }

    private void startService() {
        setupConnectionToComicsService();
        bindToMovieService();
    }

    void bindToMovieService() {
        getActivity().bindService(new Intent(getActivity(),
                ComicsService.class), comicsServiceConnection, Context.BIND_AUTO_CREATE);
        bound = true;
    }

    private void setupConnectionToComicsService() {
        comicsServiceConnection = new ServiceConnection() {
            public void onServiceConnected(ComponentName className, IBinder service) {
                comicsService = ((ComicsService.ComicsServiceBinder) service).getService();
                Log.d("Log", "Comic service connected in detail fragment");
                setRecycler();
            }

            public void onServiceDisconnected(ComponentName className) {
                comicsService = null;
                Log.d("LOG", "Comic service disconnected");

            }
        };
    }

    private void setRecycler() {
        comicsService.getMangaDetailViewmodel(comicId).observe(this, new Observer<DetailViewModel>() {
            @Override
            public void onChanged(DetailViewModel model) {
                CRVA.setData(model.getChapters());
                imageFront.setImageBitmap(model.getFrontPage());
                String temp = "";

                //TODO: Await completion
                List<String> genresList = model.getCategories();
                if (genresList != null) {
                    for (String genre : genresList) {
                        temp = temp + genre + ", ";
                    }
                    genres.setText(temp.substring(0, temp.length() - 2));
                }
                title.setText(model.getTitle());
                discription.setText(model.getDescription());
            }
        });
    }

    public String getComicId() {
        return comicId;
    }
}
