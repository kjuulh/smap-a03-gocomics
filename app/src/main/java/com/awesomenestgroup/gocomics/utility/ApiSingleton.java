package com.awesomenestgroup.gocomics.utility;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

//reference https://developer.android.com/training/volley/requestqueue#singleton
public class ApiSingleton {
    private static ApiSingleton instance;
    private static Context ctx;
    private RequestQueue requestQueue;


    public ApiSingleton(Context context) {
        ctx = context;
        requestQueue = getRequestQueue();
    }

    public static synchronized ApiSingleton getInstance(Context context) {
        if (instance == null) {
            instance = new ApiSingleton(context);
        }
        return instance;
    }

    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(ctx.getApplicationContext());
        }
        return requestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }
}
