package com.awesomenestgroup.gocomics.utility;

import com.awesomenestgroup.gocomics.models.Page;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class PageDeserializer implements JsonDeserializer<Page> {

    @Override
    public Page deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonArray object = json.getAsJsonArray();
        Page page = new Page();
        page.setPage_number(object.get(0).getAsInt());
        page.setPage_image_id(object.get(1).getAsString());
        page.setImage_width(object.get(2).getAsInt());
        page.setImage_height(object.get(3).getAsInt());
        return page;
    }
}
