package com.awesomenestgroup.gocomics.utility;

import com.awesomenestgroup.gocomics.models.ChapterModel;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class ChapterDeserializer implements JsonDeserializer<ChapterModel> {

    @Override
    public ChapterModel deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        JsonArray object = json.getAsJsonArray();
        ChapterModel chapter = new ChapterModel();
        chapter.setChapter_number(object.get(0).getAsInt());
        chapter.setDate_added(object.get(1).getAsInt());
        chapter.setChapter_name(object.get(2).toString());
        chapter.setChapter_id(object.get(3).getAsString());
        return chapter;
    }
}
