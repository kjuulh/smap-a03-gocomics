package com.awesomenestgroup.gocomics.utility;

import com.awesomenestgroup.gocomics.models.MangaDetailsModel;
import com.awesomenestgroup.gocomics.models.MangaInfoModel;
import com.awesomenestgroup.gocomics.viewModels.models.DetailViewModel;
import com.awesomenestgroup.gocomics.viewModels.models.GalleryViewModel;


public final class ViewModelBuilder {
    public static GalleryViewModel buildGalleryViewModel(MangaInfoModel rawmodel) {
        GalleryViewModel model = new GalleryViewModel();
        model.setAlias(rawmodel.getAlias());
        model.setCategory(rawmodel.getCategory());
        model.setTitle(rawmodel.getTitle());
        model.setManga_id(rawmodel.getMangaID());
        model.setLast_chapter_date(rawmodel.getLast_chapter_date());
        return model;
    }

    public static DetailViewModel buildDetailViewModel(MangaDetailsModel rawmodel, DetailViewModel original) {
        if (original == null) {
            DetailViewModel model = new DetailViewModel();
            model.setAlias(rawmodel.getAlias());
            model.setArtist(rawmodel.getArtist());
            model.setArtist_kw(rawmodel.getArtist_kw());
            model.setAuthor(rawmodel.getAuthor());
            model.setCategories(rawmodel.getCategories());
            model.setChapters(rawmodel.getChapters());
            model.setChaptersLen(rawmodel.getChaptersLen());
            model.setCreated(rawmodel.getCreated());
            model.setDescription(rawmodel.getDescription());
            model.setLast_chapter_date(rawmodel.getLast_chapter_date());
            model.setTitle(rawmodel.getTitle());
            model.setTitle_kw(rawmodel.getTitle_kw());
            return model;
        } else {
            original.setAlias(rawmodel.getAlias());
            original.setArtist(rawmodel.getArtist());
            original.setArtist_kw(rawmodel.getArtist_kw());
            original.setAuthor(rawmodel.getAuthor());
            original.setCategories(rawmodel.getCategories());
            original.setChapters(rawmodel.getChapters());
            original.setChaptersLen(rawmodel.getChaptersLen());
            original.setCreated(rawmodel.getCreated());
            original.setDescription(rawmodel.getDescription());
            original.setLast_chapter_date(rawmodel.getLast_chapter_date());
            original.setTitle(rawmodel.getTitle());
            original.setTitle_kw(rawmodel.getTitle_kw());
            return original;
        }
    }
}
