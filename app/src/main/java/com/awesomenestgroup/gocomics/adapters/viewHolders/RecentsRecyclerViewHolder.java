package com.awesomenestgroup.gocomics.adapters.viewHolders;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.awesomenestgroup.gocomics.R;

public class RecentsRecyclerViewHolder extends RecyclerView.ViewHolder {

    public TextView comicTitle;
    public TextView mostRecentChapter;
    public String mostRecentChapterId;
    public TextView currentChapter;
    public String currentChapterId;
    public TextView previousChapter;
    public String previousChapterId;
    private String TAG = "RecentsRecyclerViewHolder";

    public RecentsRecyclerViewHolder(@NonNull View itemView) {
        super(itemView);
        comicTitle = itemView.findViewById(R.id.text_recents_item_title);
        mostRecentChapter = itemView.findViewById(R.id.text_recents_item_link_recent);
        currentChapter = itemView.findViewById(R.id.text_recents_item_link_current);
        previousChapter = itemView.findViewById(R.id.text_recents_item_link_previous);

        setClickListeners();
    }

    private void setClickListeners() {
        mostRecentChapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: Clicked 1");
            }
        });

        currentChapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: Clicked 2");
            }
        });

        previousChapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: Clicked 3");
            }
        });
    }
}
