package com.awesomenestgroup.gocomics.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.awesomenestgroup.gocomics.R;
import com.awesomenestgroup.gocomics.adapters.viewHolders.GalleryRecyclerViewHolder;
import com.awesomenestgroup.gocomics.viewModels.models.GalleryViewModel;

import java.util.List;

public class GalleryRecyclerViewAdapter extends RecyclerView.Adapter<GalleryRecyclerViewHolder> {

    private final Context context;
    private List<GalleryViewModel> itemList;

    public GalleryRecyclerViewAdapter(Context context, List<GalleryViewModel> itemList) {
        this.context = context;
        this.itemList = itemList;
    }

    @NonNull
    @Override
    public GalleryRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.gallery_item, null);
        GalleryRecyclerViewHolder grvh = new GalleryRecyclerViewHolder(layoutView);
        return grvh;
    }

    @Override
    public void onBindViewHolder(@NonNull GalleryRecyclerViewHolder holder, int position) {
        holder.comicTitle.setText(itemList.get(position).getTitle());
        holder.comicView.setImageBitmap(itemList.get(position).getFrontpage());
        holder.comicId = itemList.get(position).getManga_id();
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public void setData(List<GalleryViewModel> newData) {
        this.itemList = newData;
        notifyDataSetChanged();
    }
}

