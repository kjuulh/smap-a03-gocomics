package com.awesomenestgroup.gocomics.adapters.viewHolders;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.awesomenestgroup.gocomics.R;

public class GalleryRecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public TextView comicTitle;
    public ImageView comicView;
    public String comicId;
    private String TAG = "GalleryRecyclerViewHolder";

    //TODO: Add Loader for when comicView isn't loadet

    public GalleryRecyclerViewHolder(@NonNull View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        comicTitle = itemView.findViewById(R.id.text_gallery_item_title);
        comicView = itemView.findViewById(R.id.imageView3);

    }

    @Override
    public void onClick(View v) {
        Log.d(TAG, "GalleryRecyclerViewHolder: clicked");

        Bundle comicBundle = new Bundle();
        comicBundle.putString("comicId", comicId);

        if (Navigation.findNavController(v).getCurrentDestination().getId() == R.id.gallery) {
            Navigation.findNavController(v).navigate(R.id.action_gallery_to_details, comicBundle);
        } else if (Navigation.findNavController(v).getCurrentDestination().getId() == R.id.search) {
            Navigation.findNavController(v).navigate(R.id.action_search_to_details, comicBundle);
        }
    }
}

