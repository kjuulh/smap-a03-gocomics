package com.awesomenestgroup.gocomics.adapters.viewHolders;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.awesomenestgroup.gocomics.R;

public class ChapterRecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public final TextView chapterTitle;
    public String chapterId;
    public String comicId;

    public ChapterRecyclerViewHolder(@NonNull View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        chapterTitle = itemView.findViewById(R.id.text_chapter_item_title);
    }

    @Override
    public void onClick(View v) {
        Bundle chapterBundle = new Bundle();
        chapterBundle.putString("chapterId", chapterId);
        chapterBundle.putString("comicId", comicId);
        Navigation.findNavController(v).navigate(R.id.action_details_to_readingFragment, chapterBundle);
    }
}

