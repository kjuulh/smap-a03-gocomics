package com.awesomenestgroup.gocomics.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.awesomenestgroup.gocomics.R;
import com.awesomenestgroup.gocomics.adapters.viewHolders.BookmarksRecyclerViewHolder;
import com.awesomenestgroup.gocomics.models.Bookmark;

import java.util.List;

public class BookmarksRecyclerViewAdapter extends RecyclerView.Adapter<BookmarksRecyclerViewHolder> {

    private final Context context;
    private List<Bookmark> itemList;

    public BookmarksRecyclerViewAdapter(Context context, List<Bookmark> itemList) {
        this.context = context;
        this.itemList = itemList;
    }

    @NonNull
    @Override
    public BookmarksRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.bookmark_item, null);
        BookmarksRecyclerViewHolder grvh = new BookmarksRecyclerViewHolder(layoutView);
        return grvh;
    }

    @Override
    public void onBindViewHolder(@NonNull BookmarksRecyclerViewHolder holder, int position) {

        holder.comicId = itemList.get(position).getComicId();
        holder.comicTitle.setText(itemList.get(position).getComicTitle());
        holder.mostRecentChapter.setText(itemList.get(position).getMostCurrentChapterTitle());
        holder.mostRecentChapterId = itemList.get(position).getMostCurrentChapterId();
        holder.currentChapter.setText(itemList.get(position).getLatestReadTitle());
        holder.currentChapterId = itemList.get(position).getLatestReadId();
        holder.firstChapter.setText(itemList.get(position).getFirstTitle());
        holder.firstChapterId = itemList.get(position).getFirstId();
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    /**
     * Sets the data of the related list of data, shown in the recycler
     *
     * @param newData
     */
    public void setData(List<Bookmark> newData) {
        this.itemList = newData;
        notifyDataSetChanged();
    }
}

