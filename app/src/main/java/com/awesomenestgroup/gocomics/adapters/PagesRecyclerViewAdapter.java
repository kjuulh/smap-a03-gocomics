package com.awesomenestgroup.gocomics.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.awesomenestgroup.gocomics.R;
import com.awesomenestgroup.gocomics.adapters.viewHolders.PagesRecyclerViewHolder;
import com.awesomenestgroup.gocomics.viewModels.models.PageViewModel;

import java.util.List;

public class PagesRecyclerViewAdapter extends RecyclerView.Adapter<PagesRecyclerViewHolder> {

    private final Context context;
    private List<PageViewModel> itemList;

    public PagesRecyclerViewAdapter(Context context, List<PageViewModel> itemList) {
        this.context = context;
        this.itemList = itemList;
    }

    @NonNull
    @Override
    public PagesRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.page_item, parent, false);
        return new PagesRecyclerViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull PagesRecyclerViewHolder holder, int position) {
        holder.comicView.setImageBitmap(itemList.get(position).getPage_image_id());
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public void setData(List<PageViewModel> newData) {
        this.itemList = newData;
        notifyDataSetChanged();
    }
}