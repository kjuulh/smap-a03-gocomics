package com.awesomenestgroup.gocomics.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.awesomenestgroup.gocomics.R;
import com.awesomenestgroup.gocomics.adapters.viewHolders.RecentsRecyclerViewHolder;
import com.awesomenestgroup.gocomics.models.Recent;

import java.util.List;

public class RecentsRecyclerViewAdapter extends RecyclerView.Adapter<RecentsRecyclerViewHolder> {

    private final Context context;
    private List<Recent> itemList;

    public RecentsRecyclerViewAdapter(Context context, List<Recent> itemList) {
        this.context = context;
        this.itemList = itemList;
    }

    @NonNull
    @Override
    public RecentsRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recents_item, null);
        RecentsRecyclerViewHolder grvh = new RecentsRecyclerViewHolder(layoutView);
        return grvh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecentsRecyclerViewHolder holder, int position) {
        holder.comicTitle.setText(itemList.get(position).getTitle());
        holder.mostRecentChapter.setText("Link1, do a fetch"); //TODO: DO a fetch a replace with real model
        holder.currentChapter.setText("Link2, do a fetch");
        holder.previousChapter.setText("Link3, do a fetch");
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public void setData(List<Recent> newData) {
        this.itemList = newData;
        notifyDataSetChanged();
    }
}
