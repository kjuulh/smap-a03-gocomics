package com.awesomenestgroup.gocomics.adapters.viewHolders;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.awesomenestgroup.gocomics.R;

public class BookmarksRecyclerViewHolder extends RecyclerView.ViewHolder {

    public TextView comicTitle;
    public String comicId;
    public TextView mostRecentChapter;
    public String mostRecentChapterId;
    public TextView currentChapter;
    public String currentChapterId;
    public TextView firstChapter;
    public String firstChapterId;
    private String TAG = "BookmarksRecyclerViewHolder";

    public BookmarksRecyclerViewHolder(@NonNull View itemView) {
        super(itemView);
        comicTitle = itemView.findViewById(R.id.text_bookmark_item_title);
        mostRecentChapter = itemView.findViewById(R.id.text_bookmark_item_link_recent);
        currentChapter = itemView.findViewById(R.id.text_bookmark_item_link_current);
        firstChapter = itemView.findViewById(R.id.text_bookmark_item_link_previous);

        setClickListeners();
    }

    private void setClickListeners() {
        mostRecentChapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "BookmarksRecyclerViewHolder: Clicked");

                Bundle readingBundle = getReadingBundle(mostRecentChapterId, mostRecentChapter.getText().toString());

                Navigation.findNavController(v).navigate(R.id.readingFragment, readingBundle);
            }
        });

        currentChapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "BookmarksRecyclerViewHolder: Clicked");

                Bundle readingBundle = getReadingBundle(currentChapterId, currentChapter.getText().toString());

                Navigation.findNavController(v).navigate(R.id.readingFragment, readingBundle);
            }
        });

        firstChapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "BookmarksRecyclerViewHolder: Clicked");

                Bundle readingBundle = getReadingBundle(firstChapterId, firstChapter.getText().toString());

                Navigation.findNavController(v).navigate(R.id.readingFragment, readingBundle);
            }
        });
    }

    private Bundle getReadingBundle(String chapterId, String chapterTitle) {

        Bundle tempBundle = new Bundle();
        tempBundle.putString("comicId", comicId);
        tempBundle.putString("chapterId", chapterId);
        tempBundle.putString("chapterTitle", chapterTitle);
        return tempBundle;
    }
}

