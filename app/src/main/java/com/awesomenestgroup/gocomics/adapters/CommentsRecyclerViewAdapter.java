package com.awesomenestgroup.gocomics.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.awesomenestgroup.gocomics.R;
import com.awesomenestgroup.gocomics.adapters.viewHolders.CommentRecyclerViewHolder;
import com.awesomenestgroup.gocomics.models.Comment;

import java.util.List;

public class CommentsRecyclerViewAdapter extends RecyclerView.Adapter<CommentRecyclerViewHolder> {

    private final Context context;
    private List<Comment> itemList;

    public CommentsRecyclerViewAdapter(Context context, List<Comment> itemList) {
        this.context = context;
        this.itemList = itemList;
    }

    @NonNull
    @Override
    public CommentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_item, null);
        CommentRecyclerViewHolder crvh = new CommentRecyclerViewHolder(layoutView);
        return crvh;
    }

    @Override
    public void onBindViewHolder(@NonNull CommentRecyclerViewHolder holder, int position) {
        holder.Author.setText(itemList.get(position).getUsername()); //TODO: Create hidden field with ID
        holder.Comment.setText(itemList.get(position).getCommentText());
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public void setData(List<Comment> newData) {
        this.itemList = newData;
        notifyDataSetChanged();
    }

    public void addData(Comment comment) {
        this.itemList.add(comment);
        notifyDataSetChanged();
    }
}
