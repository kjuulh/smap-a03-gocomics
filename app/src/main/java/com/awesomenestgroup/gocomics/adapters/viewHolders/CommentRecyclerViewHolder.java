package com.awesomenestgroup.gocomics.adapters.viewHolders;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.awesomenestgroup.gocomics.R;

public class CommentRecyclerViewHolder extends RecyclerView.ViewHolder {
    public final TextView Author;
    public final TextView Comment;

    public CommentRecyclerViewHolder(@NonNull View itemView) {
        super(itemView);
        Author = itemView.findViewById(R.id.text_comment_author);
        Comment = itemView.findViewById(R.id.text_comment_text);
    }
}
