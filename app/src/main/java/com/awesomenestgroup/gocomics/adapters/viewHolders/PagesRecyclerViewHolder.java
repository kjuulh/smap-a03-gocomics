package com.awesomenestgroup.gocomics.adapters.viewHolders;

import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.awesomenestgroup.gocomics.R;

public class PagesRecyclerViewHolder extends RecyclerView.ViewHolder {
    public ImageView comicView;

    public PagesRecyclerViewHolder(@NonNull View itemView) {
        super(itemView);
        comicView = itemView.findViewById(R.id.image_page_item_page);

    }
}
