package com.awesomenestgroup.gocomics.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.awesomenestgroup.gocomics.R;
import com.awesomenestgroup.gocomics.adapters.viewHolders.ChapterRecyclerViewHolder;
import com.awesomenestgroup.gocomics.models.ChapterModel;

import java.util.List;

public class ChaptersRecyclerViewAdapter extends RecyclerView.Adapter<ChapterRecyclerViewHolder> {

    private final Context context;
    private List<ChapterModel> itemList;
    private String comicId;

    public ChaptersRecyclerViewAdapter(Context context, List<ChapterModel> itemList, String comicId) {
        this.context = context;
        this.itemList = itemList;
        this.comicId = comicId;
    }

    @NonNull
    @Override
    public ChapterRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.chapter_item, parent, false);
        ChapterRecyclerViewHolder crvh = new ChapterRecyclerViewHolder(layoutView);
        return crvh;
    }

    @Override
    public void onBindViewHolder(@NonNull ChapterRecyclerViewHolder holder, int position) {
        holder.chapterTitle.setText(itemList.get(position).getChapter_name());
        holder.chapterId = itemList.get(position).getChapter_id();
        holder.comicId = comicId;
    }

    @Override
    public int getItemCount() {
        if (this.itemList != null) {
            return this.itemList.size();
        } else {
            return 0;
        }
    }

    public void setData(List<ChapterModel> newData) {
        this.itemList = newData;
        notifyDataSetChanged();
    }
}

