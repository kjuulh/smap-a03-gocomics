package com.awesomenestgroup.gocomics.viewModels.models;

import java.util.ArrayList;

public class ChapterViewModel {
    private ArrayList<PageViewModel> imagesModels;

    public ArrayList<PageViewModel> getPageImages() {
        return imagesModels;
    }

    public void setPageImages(ArrayList<PageViewModel> pageImages) {
        this.imagesModels = pageImages;
    }
}
