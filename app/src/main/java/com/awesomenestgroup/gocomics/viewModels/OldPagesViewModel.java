package com.awesomenestgroup.gocomics.viewModels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.awesomenestgroup.gocomics.viewModels.models.PageViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class OldPagesViewModel extends AndroidViewModel {

    private ExecutorService executorService;

    private List<PageViewModel> pages;
    private MutableLiveData<List<PageViewModel>> livePages;

    public OldPagesViewModel(@NonNull Application application) {
        super(application);

        pages = new ArrayList<>();
        livePages = new MutableLiveData<>();
        livePages.setValue(pages);

        executorService = Executors.newSingleThreadExecutor();
    }

    public MutableLiveData<List<PageViewModel>> getAll() {
        return livePages;
    }

    public void AddItem(@NonNull final PageViewModel page) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                pages.add(page);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                livePages.postValue(pages);
            }
        });
    }

    public void PopItem() {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                pages.remove(0);
                livePages.postValue(pages);
            }
        });
    }
}
