package com.awesomenestgroup.gocomics.viewModels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.awesomenestgroup.gocomics.models.Comment;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CommentsViewModel extends AndroidViewModel {

    private ExecutorService executorService;

    private List<Comment> comments;
    private MutableLiveData<List<Comment>> liveComments;

    public CommentsViewModel(@NonNull Application application) {
        super(application);

        comments = new ArrayList<>();
        liveComments = new MutableLiveData<>();
        liveComments.setValue(comments);

        executorService = Executors.newSingleThreadExecutor();

        addItems();
    }

    private void addItems() {
        AddItem(new Comment("That sleezy idiot", "What a comic!"));
        AddItem(new Comment("That sleezy idiot", "This must be the best"));
        AddItem(new Comment("That sleezy idiot", "Hallelujah"));
        AddItem(new Comment("That sleezy idiot", "What a comic!"));
        AddItem(new Comment("That sleezy idiot", "This must be the best"));
        AddItem(new Comment("That sleezy idiot", "Hallelujah"));
        AddItem(new Comment("That sleezy idiot", "What a comic!"));
        AddItem(new Comment("That sleezy idiot", "This must be the best"));
        AddItem(new Comment("That sleezy idiot", "Hallelujah"));
        AddItem(new Comment("That sleezy idiot", "What a comic!"));
        AddItem(new Comment("That sleezy idiot", "This must be the best"));
        AddItem(new Comment("That sleezy idiot", "Hallelujah"));
        AddItem(new Comment("That sleezy idiot", "What a comic!"));
        AddItem(new Comment("That sleezy idiot", "This must be the best"));
        AddItem(new Comment("That sleezy idiot", "Hallelujah"));
    }

    public MutableLiveData<List<Comment>> getAll() {
        return liveComments;
    }

    public void AddItem(@NonNull final Comment comment) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                comments.add(comment);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                liveComments.postValue(comments);
            }
        });
    }

    public void PopItem() {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                comments.remove(0);
                liveComments.postValue(comments);
            }
        });
    }
}

