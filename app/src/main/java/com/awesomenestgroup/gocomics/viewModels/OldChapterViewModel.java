package com.awesomenestgroup.gocomics.viewModels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.awesomenestgroup.gocomics.models.Chapter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class OldChapterViewModel extends AndroidViewModel {

    private ExecutorService executorService;

    private List<Chapter> chapters;
    private MutableLiveData<List<Chapter>> liveChapters;

    public OldChapterViewModel(@NonNull Application application) {
        super(application);

        chapters = new ArrayList<>();
        liveChapters = new MutableLiveData<>();
        liveChapters.setValue(chapters);

        executorService = Executors.newSingleThreadExecutor();

        addItems();
    }

    private void addItems() {
        AddItem(new Chapter("EP: 01 - The revenge"));
        AddItem(new Chapter("EP: 02 - The revenge"));
        AddItem(new Chapter("EP: 03 - The revenge"));
        AddItem(new Chapter("EP: 04 - The revenge"));
        AddItem(new Chapter("EP: 05 - The revenge"));
        AddItem(new Chapter("EP: 06 - The revenge"));
        AddItem(new Chapter("EP: 07 - The revenge"));
        AddItem(new Chapter("EP: 08 - The revenge"));
        AddItem(new Chapter("EP: 09 - The revenge"));
        AddItem(new Chapter("EP: 10 - The revenge"));
        AddItem(new Chapter("EP: 12 - The revenge"));
        AddItem(new Chapter("EP: 13 - The revenge"));
        AddItem(new Chapter("EP: 14 - The revenge"));
    }

    public MutableLiveData<List<Chapter>> getAll() {
        return liveChapters;
    }

    public void AddItem(@NonNull final Chapter chapter) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                chapters.add(chapter);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                liveChapters.postValue(chapters);
            }
        });
    }

    public void PopItem() {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                chapters.remove(0);
                liveChapters.postValue(chapters);
            }
        });
    }
}

