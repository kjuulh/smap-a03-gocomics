package com.awesomenestgroup.gocomics.viewModels.models;

import android.graphics.Bitmap;

import java.util.ArrayList;

public class GalleryViewModel {
    private String title;
    private Bitmap frontpage;
    private int image_width;
    private int image_height;
    private String manga_id;
    private ArrayList<String> category;
    private String last_chapter_date;
    private String alias;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Bitmap getFrontpage() {
        return frontpage;
    }

    public void setFrontpage(Bitmap frontpage) {
        this.frontpage = frontpage;
    }

    public int getImage_width() {
        return image_width;
    }

    public void setImage_width(int image_width) {
        this.image_width = image_width;
    }

    public int getImage_height() {
        return image_height;
    }

    public void setImage_height(int image_height) {
        this.image_height = image_height;
    }

    public String getManga_id() {
        return manga_id;
    }

    public void setManga_id(String manga_id) {
        this.manga_id = manga_id;
    }

    public ArrayList<String> getCategory() {
        return category;
    }

    public void setCategory(ArrayList<String> category) {
        this.category = category;
    }

    public String getLast_chapter_date() {
        return last_chapter_date;
    }

    public void setLast_chapter_date(String last_chapter_date) {
        this.last_chapter_date = last_chapter_date;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }
}
