package com.awesomenestgroup.gocomics.viewModels.models;

import android.graphics.Bitmap;

public class PageViewModel {
    private int page_number;
    private Bitmap page_image;
    private int image_width;
    private int image_height;

    public int getPage_number() {
        return page_number;
    }

    public void setPage_number(int page_number) {
        this.page_number = page_number;
    }

    public Bitmap getPage_image_id() {
        return page_image;
    }

    public void setPage_image_id(Bitmap page_image) {
        this.page_image = page_image;
    }

    public int getImage_width() {
        return image_width;
    }

    public void setImage_width(int image_width) {
        this.image_width = image_width;
    }

    public int getImage_height() {
        return image_height;
    }

    public void setImage_height(int image_height) {
        this.image_height = image_height;
    }
}
