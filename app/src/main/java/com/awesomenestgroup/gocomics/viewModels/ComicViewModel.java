package com.awesomenestgroup.gocomics.viewModels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.awesomenestgroup.gocomics.models.Comic;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ComicViewModel extends AndroidViewModel {

    private ExecutorService executorService;

    private List<Comic> comics;
    private MutableLiveData<List<Comic>> liveComics;

    public ComicViewModel(@NonNull Application application) {
        super(application);

        comics = new ArrayList<>();
        liveComics = new MutableLiveData<>();
        liveComics.setValue(comics);

        executorService = Executors.newSingleThreadExecutor();
    }

    public MutableLiveData<List<Comic>> getAllComics() {
        return liveComics;
    }

    public void AddItem(@NonNull final Comic comic) {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                comics.add(comic);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                liveComics.postValue(comics);
            }
        });
    }

    public void PopItem() {
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                comics.remove(0);
                liveComics.setValue(comics);
            }
        });
    }
}

