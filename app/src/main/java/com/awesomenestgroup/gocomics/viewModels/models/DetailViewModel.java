package com.awesomenestgroup.gocomics.viewModels.models;

import android.graphics.Bitmap;

import com.awesomenestgroup.gocomics.models.ChapterModel;

import java.util.ArrayList;
import java.util.List;

public class DetailViewModel {
    private String artist;
    private ArrayList<String> artist_kw;
    private String author;
    private ArrayList<String> alias;
    private List<String> categories;
    private ArrayList<ChapterModel> chapters;
    private String chaptersLen;
    private String created;
    private String description;
    private Bitmap image;
    private String last_chapter_date;
    private String title;
    private Bitmap frontPage;
    private ArrayList<String> title_kw;

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public ArrayList<String> getArtist_kw() {
        return artist_kw;
    }

    public void setArtist_kw(ArrayList<String> artist_kw) {
        this.artist_kw = artist_kw;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public ArrayList<ChapterModel> getChapters() {
        return chapters;
    }

    public void setChapters(ArrayList<ChapterModel> chapters) {
        this.chapters = chapters;
    }

    public String getChaptersLen() {
        return chaptersLen;
    }

    public void setChaptersLen(String chaptersLen) {
        this.chaptersLen = chaptersLen;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public String getLast_chapter_date() {
        return last_chapter_date;
    }

    public void setLast_chapter_date(String last_chapter_date) {
        this.last_chapter_date = last_chapter_date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<String> getAlias() {
        return alias;
    }

    public void setAlias(ArrayList<String> alias) {
        this.alias = alias;
    }

    public Bitmap getFrontPage() {
        return frontPage;
    }

    public void setFrontPage(Bitmap frontPage) {
        this.frontPage = frontPage;
    }

    public ArrayList<String> getTitle_kw() {
        return title_kw;
    }

    public void setTitle_kw(ArrayList<String> title_kw) {
        this.title_kw = title_kw;
    }
    //private ArrayList<String> title_kw;
}
