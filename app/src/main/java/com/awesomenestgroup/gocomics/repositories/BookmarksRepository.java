package com.awesomenestgroup.gocomics.repositories;

import android.os.AsyncTask;
import android.util.Log;

import androidx.annotation.NonNull;

import com.awesomenestgroup.gocomics.builders.BookmarksBuilder;
import com.awesomenestgroup.gocomics.listeners.OnCollectionFetchedListener;
import com.awesomenestgroup.gocomics.listeners.OnDocumentExistsListener;
import com.awesomenestgroup.gocomics.listeners.OnDocumentPostedListener;
import com.awesomenestgroup.gocomics.listeners.OnDocumentRemovedListener;
import com.awesomenestgroup.gocomics.models.Bookmark;
import com.awesomenestgroup.gocomics.models.Comic;
import com.awesomenestgroup.gocomics.providers.DatabaseProvider;
import com.awesomenestgroup.gocomics.providers.UserProvider;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

/**
 * Repository for Firestore bookmarks collection
 */
public class BookmarksRepository {

    private static BookmarksRepository instance;
    private final String BOOKMARK_COLLECTION = "bookmarks";
    private final String userId;
    private DatabaseProvider mDatabaseProvider;
    private UserProvider mUserProvider;
    private String TAG = "BookmarksRepository";
    private String USERS_COLLECTION = "users";

    private BookmarksRepository() {
        mDatabaseProvider = DatabaseProvider.getInstance();
        mUserProvider = UserProvider.getInstance();
        userId = mUserProvider.getUserId();
    }

    /**
     * Get an instance of the bookmark repository
     *
     * @return
     */
    public static BookmarksRepository getInstance() {
        if (instance == null) {
            instance = new BookmarksRepository();
        }
        return instance;
    }

    /**
     * Add a bookmark.
     */
    public void addBookmark(final Comic comic, final OnDocumentPostedListener listener) {
        class AddBookmarkTask extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {

                Map<Object, String> bookmarkObject = BookmarksBuilder.constructBookmark(comic, mUserProvider.getUser(), null);

                getDocumentReference(comic.getComicId()).set(bookmarkObject, SetOptions.merge())
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Log.d(TAG, "addBookmark: success");
                                listener.getResults(true);
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.w(TAG, "addBookmark: failure", e);
                                listener.getResults(false);
                            }
                        });
                return null;
            }
        }

        new AddBookmarkTask().execute();
    }

    public void updateLatestChapterRead(final String comicId, final Comic.ChapterKeyPair latestChapterRead, final OnDocumentPostedListener listener) {
        class UpdateBookmarkTask extends AsyncTask<Void, Void, Void> {

            protected Void doInBackground(Void... voids) {

                Map<Object, String> bookmarkObject = BookmarksBuilder.constructBookmark(comicId, mUserProvider.getUser(), latestChapterRead);

                getDocumentReference(comicId).set(bookmarkObject, SetOptions.merge())
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Log.d(TAG, "updateLatestChapterRead: success");
                                listener.getResults(true);
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.w(TAG, "updateLatestChapterRead: failure", e);
                                listener.getResults(false);
                            }
                        });
                return null;
            }
        }

        bookmarkExists(comicId, new OnDocumentExistsListener() {
            @Override
            public void getResults(Boolean success) {
                if (!success) {
                    new UpdateBookmarkTask().execute();
                }
            }
        });


    }

    /**
     * Get all bookmarks. Use the callback listener "onBookmarksFetched(ArrayList<String> bookmarks)" to receive the returned data.
     */
    public void getBookmarks(final OnCollectionFetchedListener listener) {
        class GetAllBookmarksTask extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                getCollectionReference()
                        .addSnapshotListener(new EventListener<QuerySnapshot>() {
                            @Override
                            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots,
                                                @Nullable FirebaseFirestoreException e) {
                                if (queryDocumentSnapshots != null) {
                                    List<DocumentSnapshot> documents = queryDocumentSnapshots.getDocuments();
                                    Log.d(TAG, "getBookmarks: success");

                                    List<Bookmark> bookmarks = new ArrayList<>();

                                    for (DocumentSnapshot document :
                                            documents) {
                                        Bookmark bookmark = new Bookmark();
                                        bookmark.setComicId((String) document.get("comicId"));
                                        bookmark.setComicTitle((String) document.get("comicTitle"));
                                        bookmark.setMostCurrentChapterTitle((String) document.get("mostCurrentTitle"));
                                        bookmark.setMostCurrentChapterId((String) document.get("mostCurrentId"));
                                        bookmark.setLatestReadTitle((String) document.get("latestReadTitle"));
                                        bookmark.setLatestReadId((String) document.get("latestReadId"));
                                        bookmark.setFirstTitle((String) document.get("firstTitle"));
                                        bookmark.setFirstId((String) document.get("firstId"));

                                        bookmarks.add(bookmark);
                                    }
                                    listener.getResults(bookmarks);
                                } else {
                                    Log.d(TAG, "getBookmarks: failure");
                                    listener.getResults(null);
                                }
                            }
                        });

                return null;
            }
        }

        new GetAllBookmarksTask().execute();
    }


    public void bookmarkExists(final String comicId, final OnDocumentExistsListener listener) {

        class BookmarkExistsTask extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                getDocumentReference(comicId).get()
                        .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                            @Override
                            public void onSuccess(DocumentSnapshot documentSnapshot) {

                                try {
                                    if (documentSnapshot.get("comicId").equals(comicId)) {
                                        // Remove bookmark
                                        listener.getResults(false);
                                    } else {
                                        listener.getResults(true);
                                    }
                                } catch (Exception e) {
                                    listener.getResults(true);
                                }
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                listener.getResults(null);
                            }
                        });
                return null;
            }
        }

        new BookmarkExistsTask().execute();
    }


    /**
     * Remove a specific bookmark.
     *
     * @param comicId
     */
    public void removeBookmark(final String comicId, final OnDocumentRemovedListener listener) {
        class RemoveBookmarkTask extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {

                getDocumentReference(comicId).delete()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Log.d(TAG, "addBookmark: success");
                                listener.getResults(true);
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.w(TAG, "addBookmark: failure", e);
                                listener.getResults(false);
                            }
                        });
                return null;
            }
        }

        new RemoveBookmarkTask().execute();
    }

    private DocumentReference getDocumentReference(String comicId) {
        if (comicId == null) {
            return null;
        }

        return mDatabaseProvider.getDb()
                .collection(USERS_COLLECTION).document(userId)
                .collection(BOOKMARK_COLLECTION).document(comicId);
    }

    private DocumentReference getDocumentReference() {
        return mDatabaseProvider.getDb()
                .collection(USERS_COLLECTION).document(userId)
                .collection(BOOKMARK_COLLECTION).document();
    }

    private CollectionReference getCollectionReference() {
        return mDatabaseProvider.getDb()
                .collection(USERS_COLLECTION).document(userId)
                .collection(BOOKMARK_COLLECTION);
    }

}
