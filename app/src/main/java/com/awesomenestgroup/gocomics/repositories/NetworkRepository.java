package com.awesomenestgroup.gocomics.repositories;

import android.util.Log;

import androidx.annotation.NonNull;

import com.awesomenestgroup.gocomics.providers.DatabaseProvider;
import com.awesomenestgroup.gocomics.providers.UserProvider;
import com.awesomenestgroup.gocomics.repositories.readmodels.NetworkStatusRM;
import com.awesomenestgroup.gocomics.repositories.taskcallbacks.OnRepoTaskCompleted;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.SetOptions;

import java.util.HashMap;
import java.util.Map;

/**
 * Repository for Firestore NetworkStatus collection
 */
public class NetworkRepository {

    private static NetworkRepository instance;
    private final String NETWORK_STATUS_COLLECTION = "networkstatus";
    private final String NETWORK_STATUS_SUBCOLLECTION = "networkAndStatus";
    private final String userId;
    private DatabaseProvider mDatabaseProvider;
    private UserProvider mUserProvider;
    private String TAG = "NetworkRepository";

    private NetworkRepository() {
        mDatabaseProvider = DatabaseProvider.getInstance();
        mUserProvider = UserProvider.getInstance();
        userId = mUserProvider.getUserId();
    }

    /**
     * Get an instance of the network repository
     *
     * @return
     */
    public static NetworkRepository getInstance() {
        if (instance == null) {
            instance = new NetworkRepository();
        }
        return instance;
    }

    /**
     * Add network.
     *
     * @param network
     * @param status
     */
    public void addNetworkStatus(final String network, final boolean status) {
        // Get the name of the method
        final String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();

        // Create reference to Firestore document
        DocumentReference docRef = mDatabaseProvider.getDb().collection(NETWORK_STATUS_COLLECTION).document(userId);

        // Prepare data to be added
        HashMap<String, Boolean> docData = new HashMap<>();
        docData.put(network, status);
        NetworkStatusRM networkStatusRM = new NetworkStatusRM(docData);

        // Add or update the data in Firestore
        docRef.set(networkStatusRM, SetOptions.merge())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "(" + methodName + ") Network (" + network + ") and network status (" + status + ") is added or updated in document (" + userId + ").");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "(" + methodName + ") Error while writing document", e);
                    }
                });
    }

    /**
     * Remove network.
     *
     * @param network
     */
    public void removeNetwork(final String network) {
        // Get the name of the method
        final String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();

        // Create reference to Firestore document
        DocumentReference docRef = mDatabaseProvider.getDb().collection(NETWORK_STATUS_COLLECTION).document(userId);

        // Prepare data to be removed
        Map<String, Object> updates = new HashMap<>();
        updates.put(NETWORK_STATUS_SUBCOLLECTION + "." + network, FieldValue.delete());

        // Update the data in Firestore
        docRef.update(updates)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "(" + methodName + ") Network (" + network + ") and the related status was removed in document (" + userId + ").");
                    }
                });
    }

    /**
     * Get the status for a network. Use the callback listener "onNetworkStatusFetched(boolean status)" to do stuff with the returned data.
     *
     * @param network
     * @param callback
     */
    public void getNetworkStatus(final String network, final OnRepoTaskCompleted callback) {
        // Get the name of the method
        final String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();

        // Create reference to Firestore document
        DocumentReference docRef = mDatabaseProvider.getDb().collection(NETWORK_STATUS_COLLECTION).document(userId);

        // Start Firestore get task
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                if (!task.isSuccessful()) {
                    Log.d(TAG, "(" + methodName + ") get failed with ", task.getException());
                    return;
                }
                DocumentSnapshot document = task.getResult();

                // If the document exists fetch the data
                if (document.exists()) {
                    Log.d(TAG, "(" + methodName + ") Document with chapterId " + document.getId() + " is fetched.");
                    Log.d(TAG, "(" + methodName + ") DocumentSnapshot data: " + document.getData());

                    // Map object to proper read model and do a callback to the listener
                    NetworkStatusRM networkStatusRM = document.toObject(NetworkStatusRM.class);
                    callback.onNetworkStatusFetched(networkStatusRM.getNetworkAndStatus().get(network));
                } else {
                    Log.d(TAG, "(" + methodName + ") No such document");
                }
            }
        });
    }

    /**
     * Enable a network (set the network status to true)
     *
     * @param network
     */
    public void enableNetwork(final String network) {
        // Get the name of the method
        final String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();

        // Create reference to Firestore document
        DocumentReference docRef = mDatabaseProvider.getDb().collection(NETWORK_STATUS_COLLECTION).document(userId);

        // Add or update the data in Firestore
        docRef.update(NETWORK_STATUS_SUBCOLLECTION + "." + network, true)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "(" + methodName + ") Network (" + network + ") is ENABLED and updated in document (" + userId + ").");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "(" + methodName + ") Error while writing document", e);
                    }
                });
    }

    /**
     * Disable a network (set the network status to false)
     *
     * @param userId
     * @param network
     */
    public void disableNetwork(final String userId, final String network) {
        // Get the name of the method
        final String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();

        // Create reference to Firestore document
        DocumentReference docRef = mDatabaseProvider.getDb().collection(NETWORK_STATUS_COLLECTION).document(userId);

        // Add or update the data in Firestore
        docRef.update(NETWORK_STATUS_SUBCOLLECTION + "." + network, false)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "(" + methodName + ") Network (" + network + ") is DISABLED and updated in document (" + userId + ").");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "(" + methodName + ") Error while writing document", e);
                    }
                });
    }
}
