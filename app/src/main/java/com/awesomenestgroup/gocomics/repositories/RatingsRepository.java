package com.awesomenestgroup.gocomics.repositories;

import android.util.Log;

import androidx.annotation.NonNull;

import com.awesomenestgroup.gocomics.providers.DatabaseProvider;
import com.awesomenestgroup.gocomics.providers.UserProvider;
import com.awesomenestgroup.gocomics.repositories.readmodels.RatingRM;
import com.awesomenestgroup.gocomics.repositories.taskcallbacks.OnRepoTaskCompleted;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.SetOptions;

import java.util.HashMap;

/**
 * Repository for Firestore bookmarks collection
 */
public class RatingsRepository {

    private static RatingsRepository instance;
    private final String RATING_COLLECTION = "ratings";
    private final String userId;
    private DatabaseProvider mDatabaseProvider;
    private UserProvider mUserProvider;
    private String TAG = "RatingsRepository";

    private RatingsRepository() {
        mDatabaseProvider = DatabaseProvider.getInstance();
        mUserProvider = UserProvider.getInstance();
        userId = mUserProvider.getUserId();
    }

    /**
     * Get an instance of the rating repository
     *
     * @return
     */
    public static RatingsRepository getInstance() {
        if (instance == null) {
            instance = new RatingsRepository();
        }
        return instance;
    }

    /**
     * Add rating to a comic.
     *
     * @param comicId
     * @param rating
     */
    public void addRating(final String comicId, final double rating) {
        // Get the name of the method
        final String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();

        // Create reference to Firestore document
        DocumentReference docRef = mDatabaseProvider.getDb().collection(RATING_COLLECTION).document(userId);

        // Prepare data to be added
        HashMap<String, Double> docData = new HashMap<>();
        docData.put(comicId, rating);
        RatingRM ratingRM = new RatingRM(docData);

        // Add or update the data in Firestore
        docRef.set(ratingRM, SetOptions.merge())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "(" + methodName + ") ComicId (" + comicId + ") and rating (" + rating + ") is added or updated in document (" + userId + ").");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "(" + methodName + ") Error while writing document", e);
                    }
                });
    }

    /**
     * Get a rating for a comic. Use the callback listener "onRatingFetched(double rating)" to do stuff with the returned data.
     *
     * @param comicId
     * @param callback
     */
    public void getRating(final String comicId, final OnRepoTaskCompleted callback) {
        // Get the name of the method
        final String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();

        // Create reference to Firestore document
        DocumentReference docRef = mDatabaseProvider.getDb().collection(RATING_COLLECTION).document(userId);

        // Start Firestore get task
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                if (!task.isSuccessful()) {
                    Log.d(TAG, "(" + methodName + ") get failed with ", task.getException());
                    return;
                }
                DocumentSnapshot document = task.getResult();

                // If the document exists fetch the data
                if (document.exists()) {
                    Log.d(TAG, "(" + methodName + ") Document with chapterId " + document.getId() + " is fetched.");
                    Log.d(TAG, "(" + methodName + ") DocumentSnapshot data: " + document.getData());

                    // Map object to proper read model and do a callback to the listener
                    RatingRM ratingRM = document.toObject(RatingRM.class);
                    callback.onRatingFetched(ratingRM.getComicIdsAndRatings().get(comicId));
                } else {
                    Log.d(TAG, "(" + methodName + ") No such document");
                }
            }
        });
    }
}
