package com.awesomenestgroup.gocomics.repositories;

import android.util.Log;

import androidx.annotation.NonNull;

import com.awesomenestgroup.gocomics.providers.DatabaseProvider;
import com.awesomenestgroup.gocomics.providers.UserProvider;
import com.awesomenestgroup.gocomics.repositories.readmodels.NotificationStatusRM;
import com.awesomenestgroup.gocomics.repositories.taskcallbacks.OnRepoTaskCompleted;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.SetOptions;

/**
 * Repository for Firestore Notification collection
 */
public class NotificationsRepository {

    private static NotificationsRepository instance;
    private final String NOTIFICATION_STATUS_COLLECTION = "notificationstatus";
    private final String userId;
    private DatabaseProvider mDatabaseProvider;
    private UserProvider mUserProvider;
    private String TAG = "NotificationsRepository";

    private NotificationsRepository() {
        mDatabaseProvider = DatabaseProvider.getInstance();
        mUserProvider = UserProvider.getInstance();
        userId = mUserProvider.getUserId();
    }

    /**
     * Get an instance of the notification repository
     *
     * @return
     */
    public static NotificationsRepository getInstance() {
        if (instance == null) {
            instance = new NotificationsRepository();
        }
        return instance;
    }

    /**
     * Enable notifications for user.
     */
    public void enableNotifications() {
        // Get the name of the method
        final String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();

        // Create reference to Firestore document
        DocumentReference docRef = mDatabaseProvider.getDb().collection(NOTIFICATION_STATUS_COLLECTION).document(userId);

        // Prepare data to be added
        NotificationStatusRM notificationStatusRM = new NotificationStatusRM(true);

        // Add or update the data in Firestore
        docRef.set(notificationStatusRM, SetOptions.merge())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "(" + methodName + ") Notifications is ENABLED and updated in document (" + userId + ").");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "(" + methodName + ") Error while writing document", e);
                    }
                });
    }

    /**
     * Disable notifications for user.
     */
    public void disableNotifications() {
        // Get the name of the method
        final String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();

        // Create reference to Firestore document
        DocumentReference docRef = mDatabaseProvider.getDb().collection(NOTIFICATION_STATUS_COLLECTION).document(userId);

        // Prepare data to be added
        NotificationStatusRM notificationStatusRM = new NotificationStatusRM(true);

        // Add or update the data in Firestore
        docRef.set(notificationStatusRM, SetOptions.merge())
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "(" + methodName + ") Notifications is DISABLED and updated in document (" + userId + ").");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "(" + methodName + ") Error while writing document", e);
                    }
                });
    }

    /**
     * Get the notification status for the user. Use the callback listener "onNotificationStatusFetched(boolean status)" to do stuff with the returned data.
     *
     * @param callback
     */
    public void getNotificationStatus(final OnRepoTaskCompleted callback) {
        // Get the name of the method
        final String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();

        // Create reference to Firestore document
        DocumentReference docRef = mDatabaseProvider.getDb().collection(NOTIFICATION_STATUS_COLLECTION).document(userId);

        // Start Firestore get task
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                if (!task.isSuccessful()) {
                    Log.d(TAG, "(" + methodName + ") get failed with ", task.getException());
                    return;
                }
                DocumentSnapshot document = task.getResult();

                // If the document exists fetch the data
                if (document.exists()) {
                    Log.d(TAG, "(" + methodName + ") Document with chapterId " + document.getId() + " is fetched.");
                    Log.d(TAG, "(" + methodName + ") DocumentSnapshot data: " + document.getData());

                    // Map object to proper read model and do a callback to the listener
                    NotificationStatusRM notificationStatusRM = document.toObject(NotificationStatusRM.class);
                    callback.onNotificationStatusFetched(notificationStatusRM.isEnabled());
                } else {
                    Log.d(TAG, "(" + methodName + ") No such document");
                }
            }
        });
    }
}
