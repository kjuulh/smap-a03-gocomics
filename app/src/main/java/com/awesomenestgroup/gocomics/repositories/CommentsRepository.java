package com.awesomenestgroup.gocomics.repositories;

import android.os.AsyncTask;
import android.util.Log;

import androidx.annotation.NonNull;

import com.awesomenestgroup.gocomics.builders.CommentsBuilder;
import com.awesomenestgroup.gocomics.listeners.OnCollectionFetchedListener;
import com.awesomenestgroup.gocomics.listeners.OnDocumentPostedListener;
import com.awesomenestgroup.gocomics.models.Comment;
import com.awesomenestgroup.gocomics.providers.DatabaseProvider;
import com.awesomenestgroup.gocomics.providers.UserProvider;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

/**
 * Repository for Firestore comments collection
 */
public class CommentsRepository {

    private static CommentsRepository instance;
    private final String COMICS_COLLECTION = "comics";
    private final String COMMENTS_COLLECTION = "comments";
    private final String userId;
    private String TAG = "CommentsRepository";
    private DatabaseProvider mDatabaseProvider;
    private UserProvider mUserProvider;

    private CommentsRepository() {
        mDatabaseProvider = DatabaseProvider.getInstance();
        mUserProvider = UserProvider.getInstance();
        userId = mUserProvider.getUserId();
    }

    /**
     * Get an instance of the comment repository
     *
     * @return
     */
    public static CommentsRepository getInstance() {
        if (instance == null) {
            instance = new CommentsRepository();
        }
        return instance;
    }

    /**
     * Add a comment to a comic.
     *
     * @param comicId
     * @param comment
     */
    public void addComment(final String comicId, final String comment, final OnDocumentPostedListener listener) {

        class AddCommentTask extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                // Create reference to Firestore document
                DocumentReference docRef = getDocumentReference(comicId);

                // Prepare data to be added
                Map<Object, String> commentObject = CommentsBuilder.constructComment(comment, mUserProvider.getUser());

                // Add or update the data in Firestore
                docRef.set(commentObject, SetOptions.merge())
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Log.d(TAG, "addComment: success");
                                listener.getResults(true);
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.w(TAG, "addComment: failure", e);
                                listener.getResults(false);
                            }
                        });
                return null;
            }
        }

        new AddCommentTask().execute();
    }

    /**
     * Remove a comment related to a comic.
     *
     * @param comicId
     */
    public void removeComment(final String comicId, final String documentId) {

        // Create reference to Firestore document
        getDocumentReference(comicId, documentId).delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "removeComment: success");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "removeComment ", e);
                    }
                });
    }

    /**
     * Get a comment for a comic. Use the callback listener "onCommentsFetched(String comment)" to do stuff with the returned data.
     *
     * @param comicId
     */
    public void observeComments(final String comicId, final OnCollectionFetchedListener listener) {

        class GetAllCommentsTask extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                // Create reference to Firestore document
                getCollectionReference(comicId).addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        if (queryDocumentSnapshots != null) {
                            Log.d(TAG, "observeComments: success");
                            List<DocumentSnapshot> snapshots = queryDocumentSnapshots.getDocuments();

                            List<Comment> comments = new ArrayList<>();

                            for (DocumentSnapshot snapshot :
                                    snapshots) {
                                Comment comment = new Comment((String) snapshot.get("userEmail"), (String) snapshot.get("comment"));
                                if (comment != null) {
                                    comments.add(comment);
                                }
                            }

                            listener.getResults(comments);
                        }
                    }
                });
                return null;
            }
        }

        new GetAllCommentsTask().execute();
    }

    private DocumentReference getDocumentReference(String comicId) {
        if (comicId == null) {
            return null;
        }

        return mDatabaseProvider.getDb()
                .collection(COMICS_COLLECTION).document(comicId)
                .collection(COMMENTS_COLLECTION).document();
    }

    private DocumentReference getDocumentReference(String comicId, String documentId) {
        if (comicId == null) {
            return null;
        }

        return mDatabaseProvider.getDb()
                .collection(COMICS_COLLECTION).document(comicId)
                .collection(COMMENTS_COLLECTION).document(documentId);
    }

    private CollectionReference getCollectionReference(String comicId) {
        if (comicId == null) {
            return null;
        }

        return mDatabaseProvider.getDb()
                .collection(COMICS_COLLECTION).document(comicId)
                .collection(COMMENTS_COLLECTION);
    }
}
