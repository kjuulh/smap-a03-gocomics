package com.awesomenestgroup.gocomics.repositories.readmodels;

import java.util.List;

/**
 * Firestore readmodel for Bookmark document
 */
public class BookmarksRM {
    private List<String> comicIds;

    public BookmarksRM() {
    }

    public BookmarksRM(List<String> comicIds) {
        this.comicIds = comicIds;
    }

    public List<String> getComicIds() {
        return comicIds;
    }
}