package com.awesomenestgroup.gocomics.repositories.taskcallbacks;

import java.util.ArrayList;

/**
 * Abstract class containing callback methods for Firestore repository get tasks
 */
public abstract class OnRepoTaskCompleted {
    public void onNotificationStatusFetched(boolean status) {
    }

    public void onBookmarksFetched(ArrayList<String> bookmarks) {
    }

    public void onCommentFetched(String comment) {
    }

    public void onRatingFetched(double rating) {
    }

    public void onNetworkStatusFetched(boolean status) {
    }
}