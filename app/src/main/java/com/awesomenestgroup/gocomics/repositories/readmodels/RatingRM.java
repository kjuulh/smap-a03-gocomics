package com.awesomenestgroup.gocomics.repositories.readmodels;

import java.util.HashMap;

/**
 * Firestore readmodel for Rating document
 */
public class RatingRM {
    private HashMap<String, Double> comicIdsAndRatings;

    public RatingRM() {
    }

    public RatingRM(HashMap<String, Double> comicIdsAndRatings) {
        this.comicIdsAndRatings = comicIdsAndRatings;
    }

    public HashMap<String, Double> getComicIdsAndRatings() {
        return comicIdsAndRatings;
    }
}
