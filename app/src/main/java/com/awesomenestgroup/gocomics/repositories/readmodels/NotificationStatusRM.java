package com.awesomenestgroup.gocomics.repositories.readmodels;

/**
 * Firestore readmodel for NotificationStatus document
 */
public class NotificationStatusRM {
    private boolean enabled;

    public NotificationStatusRM() {
    }

    public NotificationStatusRM(boolean isEnabled) {
        this.enabled = enabled;
    }

    public boolean isEnabled() {
        return enabled;
    }
}
