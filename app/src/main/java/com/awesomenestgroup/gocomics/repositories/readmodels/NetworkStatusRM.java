package com.awesomenestgroup.gocomics.repositories.readmodels;

import java.util.HashMap;

/**
 * Firestore readmodel for NetworkStatus document
 */
public class NetworkStatusRM {
    private HashMap<String, Boolean> networkAndStatus;

    public NetworkStatusRM() {
    }

    public NetworkStatusRM(HashMap<String, Boolean> networkAndStatus) {
        this.networkAndStatus = networkAndStatus;
    }

    public HashMap<String, Boolean> getNetworkAndStatus() {
        return networkAndStatus;
    }
}
