package com.awesomenestgroup.gocomics.repositories.readmodels;

import java.util.HashMap;

/**
 * Firestore readmodel for Comment document
 */
public class CommentsRM {
    private HashMap<String, String> comicIdsAndComments;

    public CommentsRM() {
    }

    public CommentsRM(HashMap<String, String> comicIdsAndComments) {
        this.comicIdsAndComments = comicIdsAndComments;
    }

    public HashMap<String, String> getComicIdsAndComments() {
        return comicIdsAndComments;
    }
}
