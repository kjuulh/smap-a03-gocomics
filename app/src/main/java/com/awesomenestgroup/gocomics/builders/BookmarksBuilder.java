package com.awesomenestgroup.gocomics.builders;

import com.awesomenestgroup.gocomics.models.Comic;
import com.google.firebase.auth.FirebaseUser;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class BookmarksBuilder {
    public static Map<Object, String> constructBookmark(Comic comic, FirebaseUser user, Comic.ChapterKeyPair lastestReadChapter) {

        if (comic == null || user == null) {
            return null;
        }

        Collections.reverse(comic.getChapters());

        Map<Object, String> bookmarksObject = new HashMap<>();
        bookmarksObject.put("userId", user.getUid());
        bookmarksObject.put("comicId", comic.getComicId());
        bookmarksObject.put("comicTitle", comic.getTitle());
        bookmarksObject.put("mostCurrentTitle", comic.getChapters().get(comic.getChapters().size() - 1).getTitle());
        bookmarksObject.put("mostCurrentId", comic.getChapters().get(comic.getChapters().size() - 1).getId());
        if (lastestReadChapter == null) {
            bookmarksObject.put("latestReadTitle", comic.getChapters().get(0).getTitle());
            bookmarksObject.put("latestReadId", comic.getChapters().get(0).getId());
        } else {
            bookmarksObject.put("latestReadTitle", lastestReadChapter.getTitle());
            bookmarksObject.put("latestReadTitle", lastestReadChapter.getId());
        }
        bookmarksObject.put("firstTitle", comic.getChapters().get(0).getTitle());
        bookmarksObject.put("firstId", comic.getChapters().get(0).getId());

        return bookmarksObject;
    }

    public static Map<Object, String> constructBookmark(String comicId, FirebaseUser user, Comic.ChapterKeyPair lastestReadChapter) {

        if (comicId == null || user == null) {
            return null;
        }

        Map<Object, String> bookmarksObject = new HashMap<>();
        bookmarksObject.put("userId", user.getUid());
        bookmarksObject.put("latestReadTitle", lastestReadChapter.getTitle());
        bookmarksObject.put("latestReadTitle", lastestReadChapter.getId());

        return bookmarksObject;
    }
}
