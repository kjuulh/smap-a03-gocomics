package com.awesomenestgroup.gocomics.builders;

import com.google.firebase.auth.FirebaseUser;

import java.util.HashMap;
import java.util.Map;

public class CommentsBuilder {
    public static Map<Object, String> constructComment(String comment, FirebaseUser user) {

        if (comment == null || user == null) {
            return null;
        }

        Map<Object, String> commentObject = new HashMap<>();
        commentObject.put("userEmail", user.getEmail());
        commentObject.put("userId", user.getUid());
        commentObject.put("comment", comment);
        return commentObject;
    }
}


